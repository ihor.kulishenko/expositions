package org.example.expositions;

import org.example.expositions.dao.ConnectionFactory;
import org.example.expositions.dao.DAOFactory;
import org.example.expositions.dao.ExpositionDAO;
import org.example.expositions.entity.Exposition;
import org.example.expositions.entity.ExpositionStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExpositionDAOTest {

    private ConnectionFactory connectionFactory;
    private ExpositionDAO expositionDAO;

    @BeforeEach
    public void initialize() {
        connectionFactory = new ConnectionFactory();
        expositionDAO = DAOFactory.getDAO(ExpositionDAO.class, connectionFactory);

        connectionFactory.startTransaction();
    }

    @AfterEach
    public void cleanUp() {
        connectionFactory.rollbackTransaction();
        connectionFactory.finishTransaction();
    }

    @Test
    public void createExpositionTest() {
        final var exposition = new Exposition(0L, "test-topic", LocalDate.of(2021, 5, 25), LocalDate.of(2021, 6, 26),
                new BigDecimal("100.00"), ExpositionStatus.ACTIVE);

        final var savedExposition = expositionDAO.save(exposition).orElseThrow(RuntimeException::new);

        assertTrue(expositionDAO.getById(savedExposition.getId()).isPresent());
    }

    @Test
    public void readExpositionTest() {
        final var exposition1 = new Exposition(0L, "test-topic", LocalDate.of(2021, 5, 25), LocalDate.of(2021, 6, 26),
                new BigDecimal("100.00"), ExpositionStatus.ACTIVE);

        final var savedId = expositionDAO.save(exposition1)
                .map(Exposition::getId)
                .orElseThrow(RuntimeException::new);

        final var savedExposition = expositionDAO.getById(savedId).orElseThrow(RuntimeException::new);

        assertEquals(savedId.longValue(), savedExposition.getId());
        assertEquals(exposition1.getTopic(), savedExposition.getTopic());
        assertEquals(exposition1.getStart(), savedExposition.getStart());
        assertEquals(exposition1.getFinish(), savedExposition.getFinish());
        assertEquals(exposition1.getTicketPrice(), savedExposition.getTicketPrice());
        assertEquals(exposition1.getStatus(), savedExposition.getStatus());

        // get all
        final var exposition2 = new Exposition(0L, "test-topic2", LocalDate.of(2021, 5, 25), LocalDate.of(2021, 6, 26),
                new BigDecimal("100.00"), ExpositionStatus.ACTIVE);

        final var savedExposition2 = expositionDAO.save(exposition2).orElseThrow(RuntimeException::new);

        final var expositions = expositionDAO.getAll();
        assertEquals(2, expositions.size());

        assertTrue(expositions.stream()
                .mapToLong(Exposition::getId)
                .allMatch(_id -> Set.of(savedExposition.getId(), savedExposition2.getId()).contains(_id)));

    }

    @Test
    public void updateExpositionTest() {
        final var exposition = new Exposition(0L, "test-topic", LocalDate.of(2021, 5, 25), LocalDate.of(2021, 6, 26),
                new BigDecimal("100.00"), ExpositionStatus.ACTIVE);

        final var savedExposition = expositionDAO.save(exposition)
                .map(_exposition -> expositionDAO.getById(_exposition.getId()))
                .flatMap(Function.identity())
                .orElseThrow(RuntimeException::new);

        final var updatedExposition = new Exposition(savedExposition.getId(),
                savedExposition.getTopic() + "-updated",
                savedExposition.getStart().plusDays(2),
                savedExposition.getFinish().plusDays(2),
                savedExposition.getTicketPrice().add(new BigDecimal("10.00")),
                ExpositionStatus.CANCELED);

        expositionDAO.update(updatedExposition);

        final var savedUpdateExposition = expositionDAO.getById(updatedExposition.getId())
                .orElseThrow(RuntimeException::new);

        assertEquals(updatedExposition.getId(), savedUpdateExposition.getId());
        assertEquals(updatedExposition.getTopic(), savedUpdateExposition.getTopic());
        assertEquals(updatedExposition.getStart(), savedUpdateExposition.getStart());
        assertEquals(updatedExposition.getFinish(), savedUpdateExposition.getFinish());
        assertEquals(updatedExposition.getTicketPrice(), savedUpdateExposition.getTicketPrice());
        assertEquals(updatedExposition.getStatus(), savedUpdateExposition.getStatus());
    }

    @Test
    public void deleteExpositionException() {
        final var exposition = new Exposition(0L, "test-topic", LocalDate.of(2021, 5, 25), LocalDate.of(2021, 6, 26),
                new BigDecimal("100.00"), ExpositionStatus.ACTIVE);

        final var id = expositionDAO.save(exposition)
                .map(Exposition::getId)
                .orElseThrow(RuntimeException::new);

        assertTrue(expositionDAO.getById(id).isPresent());

        expositionDAO.delete(id);

        assertFalse(expositionDAO.getById(id).isPresent());
    }

}
