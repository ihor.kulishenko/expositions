package org.example.expositions.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

class PaginatorTest {

    public static Stream<Arguments> dataForGetCurrentPage() {
        return Stream.of(
                Arguments.of(10, 3, 1),
                Arguments.of(3, 10, 1),
                Arguments.of(-1, -1, 1),
                Arguments.of(-1, 0, 1),
                Arguments.of(-1, 1, 1),
                Arguments.of(0, -1, 1),
                Arguments.of(0, 0, 1),
                Arguments.of(0, 1, 1),
                Arguments.of(1, -1, 1),
                Arguments.of(1, 0, 1)
        );
    }

    public static Stream<Arguments> dataForSetCurrentPage() {
        return Stream.of(
                Arguments.of(-1, 1),
                Arguments.of(0, 1),
                Arguments.of(1, 1),
                Arguments.of(2, 2),
                Arguments.of(3, 3),
                Arguments.of(4, 4),
                Arguments.of(5, 4)
        );
    }

    public static Stream<Arguments> dataForGetPreviousPage() {
        return Stream.of(
                Arguments.of(-2, 1),
                Arguments.of(-1, 1),
                Arguments.of(0, 1),
                Arguments.of(1, 1),
                Arguments.of(2, 1),
                Arguments.of(3, 2),
                Arguments.of(4, 3),
                Arguments.of(5, 3),
                Arguments.of(6, 3)
        );
    }

    public static Stream<Arguments> dataForGetNextPage() {
        return Stream.of(
                Arguments.of(-2, 2),
                Arguments.of(-1, 2),
                Arguments.of(0, 2),
                Arguments.of(1, 2),
                Arguments.of(2, 3),
                Arguments.of(3, 4),
                Arguments.of(4, 4),
                Arguments.of(5, 4),
                Arguments.of(6, 4)
        );
    }

    public static Stream<Arguments> dataForIsFirstPage() {
        return Stream.of(
                Arguments.of(-1, true),
                Arguments.of(0, true),
                Arguments.of(1, true),
                Arguments.of(2, false),
                Arguments.of(3, false),
                Arguments.of(4, false),
                Arguments.of(5, false)
        );
    }

    public static Stream<Arguments> dataForIsLastPage() {
        return Stream.of(
                Arguments.of(-1, false),
                Arguments.of(0, false),
                Arguments.of(1, false),
                Arguments.of(2, false),
                Arguments.of(3, false),
                Arguments.of(4, true),
                Arguments.of(5, true),
                Arguments.of(6, true)
        );
    }

    public static Stream<Arguments> dataForGetElements() {
        return Stream.of(
                Arguments.of(-1, List.of(0, 1, 2)),
                Arguments.of(0, List.of(0, 1, 2)),
                Arguments.of(1, List.of(0, 1, 2)),
                Arguments.of(2, List.of(3, 4, 5)),
                Arguments.of(3, List.of(6, 7, 8)),
                Arguments.of(4, List.of(9, 10, 11)),
                Arguments.of(5, List.of(12, 13)),
                Arguments.of(6, List.of(12, 13))
        );
    }

    @ParameterizedTest
    @MethodSource("dataForGetCurrentPage")
    void getCurrentPage(int totalElements, int elementsPerPage, int expected) {
        assertEquals(expected, new Paginator(totalElements, elementsPerPage).getCurrentPage());
    }

    @ParameterizedTest
    @MethodSource("dataForSetCurrentPage")
    void setCurrentPage(int currentPage, int expected) {
        final var paginator = new Paginator(10, 3);

        paginator.setCurrentPage(currentPage);
        assertEquals(expected, paginator.getCurrentPage());
    }

    @ParameterizedTest
    @MethodSource("dataForGetPreviousPage")
    void getPreviousPage(int currentPage, int expected) {
        final var paginator = new Paginator(10, 3);

        paginator.setCurrentPage(currentPage);

        assertEquals(expected, paginator.getPreviousPage());
    }

    @ParameterizedTest
    @MethodSource("dataForGetNextPage")
    void getNextPage(int currentPage, int expected) {
        final var paginator = new Paginator(10, 3);

        paginator.setCurrentPage(currentPage);

        assertEquals(expected, paginator.getNextPage());
    }

    @ParameterizedTest
    @MethodSource("dataForIsFirstPage")
    void isFirstPage(int currentPage, boolean expected) {
        final var paginator = new Paginator(10, 3);
        paginator.setCurrentPage(currentPage);

        assertEquals(expected, paginator.isFirstPage());
    }

    @ParameterizedTest
    @MethodSource("dataForIsLastPage")
    void isLastPage(int currentPage, boolean expected) {
        final var paginator = new Paginator(10, 3);
        paginator.setCurrentPage(currentPage);

        assertEquals(expected, paginator.isLastPage());
    }

    @ParameterizedTest
    @MethodSource("dataForGetElements")
    void getElements(int currentPage, List<Integer> expected) {
        final var list = IntStream.range(0, 14)
                .boxed()
                .collect(Collectors.toList());

        final var paginator = new Paginator(list.size(), 3);
        paginator.setCurrentPage(currentPage);

        assertIterableEquals(expected, paginator.getElements(list));
    }
}
