package org.example.expositions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.dao.ConnectionFactory;
import org.example.expositions.dao.UserDAO;
import org.example.expositions.entity.Role;
import org.example.expositions.entity.User;
import org.example.expositions.dao.DAOFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserDAOTest {
    private static final Logger logger = LogManager.getLogger();

    private ConnectionFactory connectionFactory;
    private UserDAO userDAO;

    private User testUser1;
    private User testUser2;

    @BeforeEach
    public void initialize() {
        connectionFactory = new ConnectionFactory();
        userDAO = DAOFactory.getDAO(UserDAO.class, connectionFactory);

        testUser1 = new User(0, "John", "Doe", "JohnDoe", "password", "john-doe@mail.com", Role.USER);
        testUser2 = new User(0, "Jane", "Doe", "JaneDoe", "password", "jane-doe@mail.com", Role.ADMINISTRATOR);

        connectionFactory.startTransaction();
    }

    @AfterEach
    public void cleanUp() {
        connectionFactory.rollbackTransaction();
        connectionFactory.finishTransaction();
    }

    @Test
    public void createUserTest() {
        final var createdUser1 = userDAO.save(testUser1);
        createdUser1.ifPresentOrElse(_user -> {
            assertEquals(testUser1.getFirstName(), _user.getFirstName());
            assertEquals(testUser1.getLastName(), _user.getLastName());
            assertEquals(testUser1.getLogin(), _user.getLogin());
            assertEquals(testUser1.getPassword(), _user.getPassword());
            assertEquals(testUser1.getEmail(), _user.getEmail());
            assertEquals(testUser1.getRole(), _user.getRole());
        }, Assertions::fail);
    }

    @Test
    public void getUserTest() {
        logger.info("getUserTest: start");

        final var savedUser1 = userDAO.save(testUser1).orElseThrow(RuntimeException::new);
        logger.debug("saved: " + savedUser1);

        final var returnedUser = userDAO.getById(savedUser1.getId()).orElseThrow(RuntimeException::new);
        logger.debug("returned: " + returnedUser);
        assertEquals(savedUser1.getId(), returnedUser.getId());


        final var savedUser2 = userDAO.save(testUser2).orElseThrow(RuntimeException::new);
        logger.debug("saved 2: " + savedUser2);

        final var allUsers = userDAO.getAll();
        assertEquals(2, allUsers.size());
        assertTrue(
                allUsers.stream()
                        .mapToLong(User::getId)
                        .allMatch(_id -> Set.of(savedUser1.getId(), savedUser2.getId()).contains(_id)));

        logger.info("getUserTest: finish");
    }

    @Test
    public void getByLoginTest() {
        final var savedUser = userDAO.save(testUser1).orElseThrow(RuntimeException::new);

        final var foundUser = userDAO.getByLogin(testUser1.getLogin()).orElseThrow(RuntimeException::new);

        assertEquals(savedUser.getId(), foundUser.getId());
    }

    @Test
    public void updateUserTest() {
        logger.info("updateUserTest: start");

        final var savedUser = userDAO.save(testUser1).orElseThrow(RuntimeException::new);
        logger.debug("after save: " + savedUser);
        assertEquals(testUser1.getFirstName(), savedUser.getFirstName());

        final var updatedUser = new User(savedUser.getId(), "updated-" + savedUser.getFirstName(), savedUser.getLastName(),
                savedUser.getLogin(), savedUser.getPassword(), savedUser.getEmail(), savedUser.getRole());
        userDAO.update(updatedUser);
        logger.debug("updated: " + updatedUser);

        final var updatedAfterSaveUser = userDAO.getById(updatedUser.getId()).orElseThrow(RuntimeException::new);
        assertEquals(updatedUser.getId(), updatedAfterSaveUser.getId());
        assertEquals(updatedUser.getFirstName(), updatedAfterSaveUser.getFirstName());
        logger.debug("get after update: " + updatedAfterSaveUser);

        logger.info("updateUserTest: finish");

    }

    @Test
    public void deleteUserTest() {
        logger.info("deleteUserTest: start");

        final var savedUser = userDAO.save(testUser1).orElseThrow(RuntimeException::new);
        logger.debug("after save: " + savedUser);

        assertTrue(userDAO.getById(savedUser.getId()).isPresent());

        userDAO.delete(savedUser.getId());

        assertFalse(userDAO.getById(savedUser.getId()).isPresent());

        logger.info("deleteUserTest: finish");
    }
}
