package org.example.expositions.factory;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ExpositionFactoryTest {

    @Test
    void createDTO() throws IOException {
        final var jsonString = "{\"topic\":\"hello world\",\"dateFrom\":\"2022-01-02\",\"dateTo\":\"2022-01-09\",\"halls\":[196],\"schedule\":[{\"weekday\":\"MONDAY\",\"timeFrom\":\"09:00\",\"timeTo\":\"18:00\"}],\"price\":\"120\"}";
        final var dto = ExpositionFactory.createDTO(jsonString);

        assertNotNull(dto);

        assertEquals("hello world", dto.getTopic());
        assertEquals(LocalDate.of(2022, 1, 2), dto.getDateFrom());
        assertEquals(LocalDate.of(2022, 1, 9), dto.getDateTo());
        assertEquals(1, dto.getHalls().size());
        assertEquals(1, dto.getSchedule().size());
        assertEquals(new BigDecimal("120"), dto.getTicketPrice());
    }
}
