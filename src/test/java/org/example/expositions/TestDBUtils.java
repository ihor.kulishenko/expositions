package org.example.expositions;


import org.example.expositions.dao.ConnectionFactory;

public class TestDBUtils {
    public static void clearTable(TABLES table) {
        final var connectionFactory = new ConnectionFactory();

        try (final var ps = connectionFactory.getConnection().prepareStatement(table.clearSql)) {
            ps.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            connectionFactory.closeConnection();
        }
    }

    public enum TABLES {
        USER("DELETE FROM public.\"user\" WHERE TRUE"),
        EXPOSITION_HALL("DELETE FROM public.exposition_hall WHERE TRUE"),
        EXPOSITION("DELETE FROM public.exposition WHERE TRUE"),
        HALL("DELETE FROM public.hall WHERE TRUE"),
        SCHEDULE("DELETE FROM public.schedule WHERE TRUE"),
        TICKET("DELETE FROM public.ticket WHERE TRUE");

        private final String clearSql;

        TABLES(String clearSql) {
            this.clearSql = clearSql;
        }
    }
}
