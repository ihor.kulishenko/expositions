package org.example.expositions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.dao.ConnectionFactory;
import org.example.expositions.dao.ExpositionDAO;
import org.example.expositions.dao.ScheduleDAO;
import org.example.expositions.entity.Exposition;
import org.example.expositions.entity.ExpositionStatus;
import org.example.expositions.entity.Schedule;
import org.example.expositions.dao.DAOFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ScheduleDAOTest {
    private static final Logger logger = LogManager.getLogger();

    private ConnectionFactory connectionFactory;
    private ScheduleDAO scheduleDAO;

    private Exposition testExposition1;
    private Exposition testExposition2;

    @BeforeEach
    public void init() {

        connectionFactory = new ConnectionFactory();
        final var expositionDAO = DAOFactory.getDAO(ExpositionDAO.class, connectionFactory);
        scheduleDAO = DAOFactory.getDAO(ScheduleDAO.class, connectionFactory);

        final var exposition1 = new Exposition(0L, "topic-test", LocalDate.of(2020, 6, 12), LocalDate.of(2020, 7, 13),
                BigDecimal.TEN, ExpositionStatus.ACTIVE);

        final var exposition2 = new Exposition(0L, "topic2-test", LocalDate.of(2020, 9, 12), LocalDate.of(2020, 10, 13),
                BigDecimal.TEN, ExpositionStatus.ACTIVE);

        connectionFactory.startTransaction();

        testExposition1 = expositionDAO.save(exposition1).orElseThrow(RuntimeException::new);
        testExposition2 = expositionDAO.save(exposition2).orElseThrow(RuntimeException::new);
    }

    @AfterEach
    public void cleanUp() {
        connectionFactory.rollbackTransaction();
        connectionFactory.finishTransaction();
    }

    @Test
    public void createScheduleTest() {

        final var schedule = new Schedule(0L, testExposition1.getId(), DayOfWeek.SATURDAY, LocalTime.NOON, LocalTime.MIDNIGHT);
        final var savedSchedule = scheduleDAO.save(schedule).orElseThrow(RuntimeException::new);
        logger.debug("createScheduleTest: saved schedule: " + savedSchedule);

        assertEquals(schedule.getExpositionId(), savedSchedule.getExpositionId());
        assertEquals(schedule.getDayOfWeek(), savedSchedule.getDayOfWeek());
        assertEquals(schedule.getStart(), savedSchedule.getStart());
        assertEquals(schedule.getFinish(), savedSchedule.getFinish());

    }

    @Test
    public void readByIdTest() {
        final var schedule = new Schedule(0L, testExposition1.getId(), DayOfWeek.SATURDAY, LocalTime.NOON, LocalTime.MIDNIGHT);
        final var savedSchedule = scheduleDAO.save(schedule).orElseThrow(RuntimeException::new);

        final var returnedAfterSave = scheduleDAO.getById(savedSchedule.getId()).orElseThrow(RuntimeException::new);
        logger.debug("readByIdTest: saved schedule: " + returnedAfterSave);


        assertEquals(savedSchedule.getId(), returnedAfterSave.getId());
        assertEquals(savedSchedule.getExpositionId(), returnedAfterSave.getExpositionId());
        assertEquals(savedSchedule.getDayOfWeek(), returnedAfterSave.getDayOfWeek());
        assertEquals(savedSchedule.getStart(), returnedAfterSave.getStart());
        assertEquals(savedSchedule.getFinish(), returnedAfterSave.getFinish());

    }

    @Test
    public void readAllTest() {

        final var schedule1 = new Schedule(0L, testExposition1.getId(), DayOfWeek.SATURDAY, LocalTime.NOON, LocalTime.MIDNIGHT);
        final var savedSchedule1 = scheduleDAO.save(schedule1).orElseThrow(RuntimeException::new);

        final var schedule2 = new Schedule(0L, testExposition2.getId(), DayOfWeek.MONDAY, LocalTime.NOON, LocalTime.MIDNIGHT);
        final var savedSchedule2 = scheduleDAO.save(schedule2).orElseThrow(RuntimeException::new);

        final var schedules = scheduleDAO.getAll();

        assertTrue(schedules.stream()
                .anyMatch(_s -> Set.of(savedSchedule1.getId(), savedSchedule2.getId()).contains(_s.getId())));

    }

    @Test
    public void readByExpositionId() {

        final var schedule1 = new Schedule(0L, testExposition1.getId(), DayOfWeek.SATURDAY, LocalTime.NOON, LocalTime.MIDNIGHT);
        scheduleDAO.save(schedule1).orElseThrow(RuntimeException::new);

        final var schedule2 = new Schedule(0L, testExposition2.getId(), DayOfWeek.MONDAY, LocalTime.NOON, LocalTime.MIDNIGHT);
        final var savedSchedule2 = scheduleDAO.save(schedule2).orElseThrow(RuntimeException::new);

        final var returnedAfterSave = scheduleDAO.getByExpositionId(testExposition2.getId());

        assertEquals(1, returnedAfterSave.size());
        assertEquals(testExposition2.getId(), returnedAfterSave.get(0).getExpositionId());
        assertEquals(savedSchedule2.getId(), returnedAfterSave.get(0).getId());
    }

    @Test
    public void updateTest() {

        final var schedule1 = new Schedule(0L, testExposition1.getId(), DayOfWeek.SATURDAY, LocalTime.NOON, LocalTime.MIDNIGHT);
        final var savedSchedule = scheduleDAO.save(schedule1).orElseThrow(RuntimeException::new);

        final var updateSchedule = new Schedule(savedSchedule.getId(), savedSchedule.getExpositionId(),
                DayOfWeek.MONDAY, LocalTime.MIDNIGHT, LocalTime.NOON);

        scheduleDAO.update(updateSchedule);

        final var returnedAfterUpdate = scheduleDAO.getById(updateSchedule.getId()).orElseThrow(RuntimeException::new);

        assertEquals(updateSchedule.getId(), returnedAfterUpdate.getId());
        assertEquals(updateSchedule.getExpositionId(), returnedAfterUpdate.getExpositionId());
        assertEquals(DayOfWeek.SATURDAY, returnedAfterUpdate.getDayOfWeek());
        assertEquals(updateSchedule.getStart(), returnedAfterUpdate.getStart());
        assertEquals(updateSchedule.getFinish(), returnedAfterUpdate.getFinish());
    }

    @Test
    public void deleteTest() {

        final var schedule1 = new Schedule(0L, testExposition1.getId(), DayOfWeek.SATURDAY, LocalTime.NOON, LocalTime.MIDNIGHT);
        final var savedSchedule = scheduleDAO.save(schedule1).orElseThrow(RuntimeException::new);

        assertTrue(scheduleDAO.getById(savedSchedule.getId()).isPresent());

        scheduleDAO.delete(savedSchedule.getId());

        assertFalse(scheduleDAO.getById(savedSchedule.getId()).isPresent());
    }

}
