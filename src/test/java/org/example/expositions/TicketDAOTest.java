package org.example.expositions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.dao.*;
import org.example.expositions.entity.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TicketDAOTest {

    private static final Logger logger = LogManager.getLogger();

    private ConnectionFactory connectionFactory;

    private TicketDAO ticketDAO;

    private User testUser1;
    private User testUser2;

    private Exposition testExposition1;
    private Exposition testExposition2;

    @BeforeEach
    public void init() {

        connectionFactory = new ConnectionFactory();

        ticketDAO = DAOFactory.getDAO(TicketDAO.class, connectionFactory);

        UserDAO userDAO = DAOFactory.getDAO(UserDAO.class, connectionFactory);
        ExpositionDAO expositionDAO = DAOFactory.getDAO(ExpositionDAO.class, connectionFactory);

        final var user1 = new User(0L, "testFirstName1", "testLastName1", "test-login1", "test-password",
                "test1@email.org", Role.USER);

        final var user2 = new User(0L, "testFirstName2", "testLastName2", "test-login2", "test-password",
                "test2@email.org", Role.ADMINISTRATOR);

        final var exposition1 = new Exposition(0L, "test-topic1", LocalDate.EPOCH, LocalDate.now(), BigDecimal.TEN,
                ExpositionStatus.ACTIVE);

        final var exposition2 = new Exposition(0L, "test-topic2", LocalDate.EPOCH, LocalDate.now(), new BigDecimal("20.00"),
                ExpositionStatus.ACTIVE);

        connectionFactory.startTransaction();

        testUser1 = userDAO.save(user1).orElseThrow(RuntimeException::new);
        testUser2 = userDAO.save(user2).orElseThrow(RuntimeException::new);

        testExposition1 = expositionDAO.save(exposition1).orElseThrow(RuntimeException::new);
        testExposition2 = expositionDAO.save(exposition2).orElseThrow(RuntimeException::new);
    }

    @AfterEach
    public void cleanUp() {
        connectionFactory.rollbackTransaction();
        connectionFactory.finishTransaction();
    }

    @Test
    public void createTest() {
        final var ticket = new Ticket(0L, testUser1, testExposition1, LocalDate.EPOCH, BigDecimal.TEN);

        final var savedTicket = ticketDAO.save(ticket).orElseThrow(RuntimeException::new);
        logger.debug("createTest: saved ticket: " + savedTicket);

        assertEquals(ticket.getUser().getId(), savedTicket.getUser().getId());
        assertEquals(ticket.getExposition().getId(), savedTicket.getExposition().getId());
        assertEquals(ticket.getDate(), savedTicket.getDate());
    }

    @Test
    public void readTest() {
        final var ticket = new Ticket(0L, testUser1, testExposition1, LocalDate.EPOCH, BigDecimal.TEN);
        final var savedTicket = ticketDAO.save(ticket).orElseThrow(RuntimeException::new);

        final var returnedTicket = ticketDAO.getById(savedTicket.getId()).orElseThrow(RuntimeException::new);

        assertEquals(savedTicket.getId(), returnedTicket.getId());
        assertEquals(savedTicket.getUser().getId(), returnedTicket.getUser().getId());
        assertEquals(savedTicket.getExposition().getId(), returnedTicket.getExposition().getId());
        assertEquals(savedTicket.getDate(), returnedTicket.getDate());

        //
        final var ticket2 = new Ticket(0L, testUser1, testExposition1, LocalDate.EPOCH.plusDays(1), BigDecimal.TEN);
        final var savedTicket2 = ticketDAO.save(ticket2).orElseThrow(RuntimeException::new);

        final var tickets = ticketDAO.getAll();

        assertTrue(tickets.stream().anyMatch(_t -> Set.of(savedTicket.getId(), savedTicket2.getId()).contains(_t.getId())));

    }

    @Test
    public void updateTest() {
        final var ticket = new Ticket(0L, testUser1, testExposition1, LocalDate.EPOCH, BigDecimal.TEN);
        final var savedTicket = ticketDAO.save(ticket).orElseThrow(RuntimeException::new);

        final var updateTicket = new Ticket(savedTicket.getId(), testUser2, testExposition2,
                savedTicket.getDate().plusDays(10), BigDecimal.TEN);
        ticketDAO.update(updateTicket);

        final var returnedAfterUpdateTicket = ticketDAO.getById(savedTicket.getId()).orElseThrow(RuntimeException::new);

        assertEquals(updateTicket.getId(), returnedAfterUpdateTicket.getId());
        assertEquals(updateTicket.getUser().getId(), returnedAfterUpdateTicket.getUser().getId());
        assertEquals(updateTicket.getExposition().getId(), returnedAfterUpdateTicket.getExposition().getId());
        assertEquals(updateTicket.getDate(), returnedAfterUpdateTicket.getDate());
    }

    @Test
    public void deleteTest() {
        final var ticket = new Ticket(0L, testUser1, testExposition1, LocalDate.EPOCH, BigDecimal.TEN);
        final var savedTicket = ticketDAO.save(ticket).orElseThrow(RuntimeException::new);

        assertTrue(ticketDAO.getById(savedTicket.getId()).isPresent());

        ticketDAO.delete(savedTicket.getId());

        assertFalse(ticketDAO.getById(savedTicket.getId()).isPresent());

    }
}
