package org.example.expositions.delegate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.TestDBUtils;
import org.example.expositions.entity.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class ExpositionDelegateTest {
    private static final Logger logger = LogManager.getLogger();

    private ExpositionDelegate expositionDelegate;
    private HallDelegate hallDelegate;
    private ScheduleDelegate scheduleDelegate;

    private User testUser;
    private List<Hall> testHalls;
    private List<Schedule> testSchedule;
    private Exposition testExposition;

    @BeforeEach
    void setUp() {
        TestDBUtils.clearTable(TestDBUtils.TABLES.EXPOSITION_HALL);
        TestDBUtils.clearTable(TestDBUtils.TABLES.SCHEDULE);
        TestDBUtils.clearTable(TestDBUtils.TABLES.EXPOSITION);
        TestDBUtils.clearTable(TestDBUtils.TABLES.USER);
        TestDBUtils.clearTable(TestDBUtils.TABLES.HALL);

        expositionDelegate = new ExpositionDelegate();
        UserDelegate userDelegate = new UserDelegate();
        hallDelegate = new HallDelegate();
        scheduleDelegate = new ScheduleDelegate();

        // create test user
        final var userResult = userDelegate.create(new User(0L, "test", "user", "test-user", "test-user",
                "test-user@mail.org", Role.ADMINISTRATOR));
        if (userResult.failure()) {
            fail();
        } else {
            testUser = userResult.container;
        }

        // create test halls
        testHalls = new ArrayList<>();

        final var halls = List.of(new Hall(0L, "test hall 1"), new Hall(0L, "test hall 2"), new Hall(0L, "test hall 3"));
        for (Hall hall : halls) {
            final var result = hallDelegate.create(hall);
            if (result.success()) {
                testHalls.add(result.container);
            } else {
                throw new RuntimeException(result.message);
            }
        }

        // test schedule
        testSchedule = new ArrayList<>();
        testSchedule.add(new Schedule(0L, 0L, DayOfWeek.MONDAY, LocalTime.of(9, 0), LocalTime.of(18, 0)));
        testSchedule.add(new Schedule(0L, 0L, DayOfWeek.TUESDAY, LocalTime.of(9, 0), LocalTime.of(18, 0)));
        testSchedule.add(new Schedule(0L, 0L, DayOfWeek.WEDNESDAY, LocalTime.of(9, 0), LocalTime.of(18, 0)));

        // test exposition
        testExposition = new Exposition(0L, "test-topic", LocalDate.of(2021, 12, 6), LocalDate.of(2021, 12, 8),
                BigDecimal.TEN, ExpositionStatus.ACTIVE);
    }

    @AfterEach
    void tearDown() {
        TestDBUtils.clearTable(TestDBUtils.TABLES.EXPOSITION_HALL);
        TestDBUtils.clearTable(TestDBUtils.TABLES.SCHEDULE);
        TestDBUtils.clearTable(TestDBUtils.TABLES.EXPOSITION);
        TestDBUtils.clearTable(TestDBUtils.TABLES.USER);
        TestDBUtils.clearTable(TestDBUtils.TABLES.HALL);
    }

    @Test
    public void createExpositionTest() {

        final var expositionResult = expositionDelegate.create(testExposition, testHalls, testSchedule, testUser);
        assertTrue(expositionResult.success());

        logger.info("exposition created: {}", expositionResult.container);

        // schedule
        final var scheduleResult = scheduleDelegate.getByExposition(expositionResult.container);
        assertTrue(scheduleResult.success());
        logger.info("saved schedule: {}", scheduleResult.container);

        // hall
        final var hallsResult = hallDelegate.getByExposition(expositionResult.container);
        assertTrue(hallsResult.success());
        logger.info("halls attached to exposition: {}", hallsResult.container);
    }
}
