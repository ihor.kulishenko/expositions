package org.example.expositions.delegate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.TestDBUtils;
import org.example.expositions.entity.Role;
import org.example.expositions.entity.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class UserDelegateTest {

    private static final Logger logger = LogManager.getLogger();

    private UserDelegate userDelegate;
    private User testUser;

    private static Stream<Arguments> createUserTestBadDataProvider() {
        return Stream.of(
                arguments(new User(0L, null, "abc", "abc", "abc", "abc", Role.USER)),
                arguments(new User(0L, "abc", null, "abc", "abc", "abc", Role.USER)),
                arguments(new User(0L, "abc", "abc", null, "abc", "abc", Role.USER)),
                arguments(new User(0L, "abc", "abc", "abc", null, "abc", Role.USER)),
                arguments(new User(0L, "abc", "abc", "abc", "abc", null, Role.USER)),
                arguments(new User(0L, "abc", "abc", "abc", "abc", "abc", null)),

                arguments(new User(0L, "", "abc", "abc", "abc", "abc", Role.USER)),
                arguments(new User(0L, "abc", "", "abc", "abc", "abc", Role.USER)),
                arguments(new User(0L, "abc", "abc", "", "abc", "abc", Role.USER)),
                arguments(new User(0L, "abc", "abc", "abc", "", "abc", Role.USER)),
                arguments(new User(0L, "abc", "abc", "abc", "abc", "", Role.USER)),

                arguments(new User(0L, "   ", "abc", "abc", "abc", "abc", Role.USER)),
                arguments(new User(0L, "abc", "   ", "abc", "abc", "abc", Role.USER)),
                arguments(new User(0L, "abc", "abc", "   ", "abc", "abc", Role.USER)),
                arguments(new User(0L, "abc", "abc", "abc", "   ", "abc", Role.USER)),
                arguments(new User(0L, "abc", "abc", "abc", "abc", "   ", Role.USER))
        );
    }

    @BeforeEach
    public void init() {
        userDelegate = new UserDelegate();

        testUser = new User(0L, "testFirstName", "testLastName", "testLogin", "testPassword", "email@mail.org", Role.USER);

        TestDBUtils.clearTable(TestDBUtils.TABLES.USER);
    }

    @AfterEach
    public void clearUp() {
        TestDBUtils.clearTable(TestDBUtils.TABLES.USER);
    }

    @ParameterizedTest
    @MethodSource("createUserTestBadDataProvider")
    public void createUserWithBadDataTest(User user) {
        final var result = userDelegate.create(user);
        assertTrue(result.failure());
    }

    @Test
    public void checkDuplicateLoginTest() {
        final var createdUser = userDelegate.create(testUser);

        assertTrue(createdUser.success());

        logger.debug("createUserWithBadDataTest: user {} created", createdUser.container);

        final var createdUser2 = userDelegate.create(testUser);

        assertFalse(createdUser2.success());
    }

    @Test
    public void createUserTest() {
        final var createdUser = userDelegate.create(testUser);

        assertTrue(createdUser.success());
    }
}
