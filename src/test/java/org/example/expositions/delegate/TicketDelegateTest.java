package org.example.expositions.delegate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.TestDBUtils;
import org.example.expositions.entity.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

class TicketDelegateTest {

    private static final Logger logger = LogManager.getLogger();

    private TicketDelegate ticketDelegate;

    private Exposition testExposition;
    private User testUser;


    @BeforeEach
    void setUp() {
        TestDBUtils.clearTable(TestDBUtils.TABLES.EXPOSITION_HALL);
        TestDBUtils.clearTable(TestDBUtils.TABLES.SCHEDULE);
        TestDBUtils.clearTable(TestDBUtils.TABLES.TICKET);
        TestDBUtils.clearTable(TestDBUtils.TABLES.EXPOSITION);
        TestDBUtils.clearTable(TestDBUtils.TABLES.USER);
        TestDBUtils.clearTable(TestDBUtils.TABLES.HALL);

        //
        ticketDelegate = new TicketDelegate();

        /// create exposition

        final ExpositionDelegate expositionDelegate = new ExpositionDelegate();
        final HallDelegate hallDelegate = new HallDelegate();
        final UserDelegate userDelegate = new UserDelegate();

        // create user
        testUser = null;
        final var userResult = userDelegate.create(new User(0L, "test", "user", "test-user", "test-user",
                "test-user@mail.org", Role.ADMINISTRATOR));
        if (userResult.failure()) {
            fail();
        }
        testUser = userResult.container;
        logger.info("user {} created", testUser);

        // create halls
        List<Hall> testHalls = new ArrayList<>();

        final var halls = List.of(new Hall(0L, "test hall 1"), new Hall(0L, "test hall 2"), new Hall(0L, "test hall 3"));
        for (Hall hall : halls) {
            final var result = hallDelegate.create(hall);
            if (result.success()) {
                testHalls.add(result.container);
                logger.info("hall {} created", result.container);
            } else {
                throw new RuntimeException(result.message);
            }
        }

        // create schedule
        List<Schedule> testSchedule = new ArrayList<>();
        testSchedule.add(new Schedule(0L, 0L, DayOfWeek.MONDAY, LocalTime.of(9, 0), LocalTime.of(18, 0)));
        testSchedule.add(new Schedule(0L, 0L, DayOfWeek.TUESDAY, LocalTime.of(9, 0), LocalTime.of(18, 0)));
        testSchedule.add(new Schedule(0L, 0L, DayOfWeek.WEDNESDAY, LocalTime.of(9, 0), LocalTime.of(18, 0)));


        // create exposition
        testExposition = new Exposition(0L, "test-topic", LocalDate.of(2021, 12, 6), LocalDate.of(2021, 12, 8),
                BigDecimal.TEN, ExpositionStatus.ACTIVE);
        final var expositionResult = expositionDelegate.create(testExposition, testHalls, testSchedule, testUser);
        if (expositionResult.failure()) {
            fail();
        }
        testExposition = expositionResult.container;
        logger.info("exposition {} created", testExposition);
    }

    @AfterEach
    void tearDown() {
        TestDBUtils.clearTable(TestDBUtils.TABLES.EXPOSITION_HALL);
        TestDBUtils.clearTable(TestDBUtils.TABLES.SCHEDULE);
        TestDBUtils.clearTable(TestDBUtils.TABLES.TICKET);
        TestDBUtils.clearTable(TestDBUtils.TABLES.EXPOSITION);
        TestDBUtils.clearTable(TestDBUtils.TABLES.USER);
        TestDBUtils.clearTable(TestDBUtils.TABLES.HALL);
    }

    @Test
    void createTest() {
        final var testTicket = new Ticket(0L, testUser, testExposition, LocalDate.of(2021, 12, 7), BigDecimal.ZERO);
        final var ticketResult = ticketDelegate.create(testTicket);
        assertTrue(ticketResult.success());
        assertEquals(testExposition.getTicketPrice(), ticketResult.container.getPrice());

        logger.info("ticket {} created", ticketResult.container);
    }
}
