package org.example.expositions;

import org.example.expositions.dao.ConnectionFactory;
import org.example.expositions.dao.HallDAO;
import org.example.expositions.entity.Hall;
import org.example.expositions.dao.DAOFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HallDAOTest {
    private ConnectionFactory connectionFactory;
    private HallDAO hallDAO;

    @BeforeEach
    public void initialize() {
        connectionFactory = new ConnectionFactory();
        hallDAO = DAOFactory.getDAO(HallDAO.class, connectionFactory);

        connectionFactory.startTransaction();
    }

    @AfterEach
    public void cleanUp() {
        connectionFactory.rollbackTransaction();
        connectionFactory.closeConnection();
    }

    @Test
    public void createHallTest() {

        final var hall0 = new Hall(0, "testHall");

        final var savedHall = hallDAO.save(hall0).orElseThrow(RuntimeException::new);
        final var returnedById = hallDAO.getById(savedHall.getId()).orElseThrow(RuntimeException::new);

        assertEquals(savedHall.getId(), returnedById.getId());
        assertEquals(savedHall.getName(), returnedById.getName());
    }

    @Test
    public void readHallTest() {
        final var hall0 = new Hall(0, "testHall-1");
        final var savedHall0 = hallDAO.save(hall0).orElseThrow(RuntimeException::new);
        hallDAO.getById(savedHall0.getId()).orElseThrow(RuntimeException::new);

        final var hall1 = new Hall(0, "testHall-2");
        final var savedHall1 = hallDAO.save(hall1).orElseThrow(RuntimeException::new);
        hallDAO.getById(savedHall1.getId()).orElseThrow(RuntimeException::new);

        ///
        final var halls = hallDAO.getAll();

        assertTrue(halls.stream()
                .mapToLong(Hall::getId)
                .anyMatch(_hallId -> Set.of(savedHall0.getId(), savedHall1.getId()).contains(_hallId)));
    }

    @Test
    public void updateHallTest() {
        final var hall0 = new Hall(0, "testHall-1");
        final var savedHall0 = hallDAO.save(hall0).orElseThrow(RuntimeException::new);

        final var updatedHall = new Hall(savedHall0.getId(), savedHall0.getName() + "-updated");
        hallDAO.update(updatedHall);

        final var savedAfterUpdateHall = hallDAO.getById(savedHall0.getId()).orElseThrow(RuntimeException::new);
        assertEquals(savedHall0.getId(), savedAfterUpdateHall.getId());
        assertEquals(updatedHall.getName(), savedAfterUpdateHall.getName());

    }

    @Test
    public void deleteHallTest() {
        final var savedHall0 = hallDAO.save(new Hall(0, "testHall-1")).orElseThrow(RuntimeException::new);

        assertTrue(hallDAO.getById(savedHall0.getId()).isPresent());

        hallDAO.delete(savedHall0.getId());

        assertFalse(hallDAO.getById(savedHall0.getId()).isPresent());

    }
}
