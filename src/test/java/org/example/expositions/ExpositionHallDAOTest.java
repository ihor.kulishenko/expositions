package org.example.expositions;

import org.example.expositions.dao.*;
import org.example.expositions.entity.Exposition;
import org.example.expositions.entity.ExpositionStatus;
import org.example.expositions.entity.Hall;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExpositionHallDAOTest {

    private ConnectionFactory connectionFactory;
    private ExpositionHallDAO expositionHallDAO;

    private Hall testHall1;
    private Hall testHall2;
    private Hall testHall3;

    private Exposition testExposition1;
    private Exposition testExposition2;

    @BeforeEach
    public void init() {
        connectionFactory = new ConnectionFactory();

        expositionHallDAO = DAOFactory.getDAO(ExpositionHallDAO.class, connectionFactory);
        final var expositionDAO = DAOFactory.getDAO(ExpositionDAO.class, connectionFactory);
        final var hallDAO = DAOFactory.getDAO(HallDAO.class, connectionFactory);

        connectionFactory.startTransaction();

        testHall1 = hallDAO.save(new Hall(0L, "test-hall-1")).orElseThrow(RuntimeException::new);
        testHall2 = hallDAO.save(new Hall(0L, "test-hall-2")).orElseThrow(RuntimeException::new);
        testHall3 = hallDAO.save(new Hall(0L, "test-hall-3")).orElseThrow(RuntimeException::new);

        final var exp1 = new Exposition(0L, "test-topic-1", LocalDate.EPOCH, LocalDate.now(),
                BigDecimal.TEN, ExpositionStatus.ACTIVE);
        testExposition1 = expositionDAO.save(exp1).orElseThrow(RuntimeException::new);

        final var exp2 = new Exposition(0L, "test-topic-2", LocalDate.EPOCH, LocalDate.now(),
                BigDecimal.TEN, ExpositionStatus.ACTIVE);

        testExposition2 = expositionDAO.save(exp2).orElseThrow(RuntimeException::new);
    }

    @AfterEach
    public void cleanUp() {
        connectionFactory.rollbackTransaction();
        connectionFactory.finishTransaction();
    }

    @Test
    public void saveTest() {
        expositionHallDAO.save(testExposition1, testHall1);

        final var halls = expositionHallDAO.get(testExposition1);
        assertEquals(1, halls.size());
        assertEquals(testHall1.getId(), halls.get(0).getId());
    }

    @Test
    public void getTest() {
        expositionHallDAO.save(testExposition1, testHall1);
        expositionHallDAO.save(testExposition1, testHall2);

        expositionHallDAO.save(testExposition2, testHall2);
        expositionHallDAO.save(testExposition2, testHall3);

        final var halls1 = expositionHallDAO.get(testExposition1);
        assertEquals(2, halls1.size());
        assertTrue(halls1.stream().allMatch(_h -> Set.of(testHall1.getId(), testHall2.getId()).contains(_h.getId())));

        final var halls2 = expositionHallDAO.get(testExposition2);
        assertEquals(2, halls2.size());
        assertTrue(halls2.stream().allMatch(_h -> Set.of(testHall2.getId(), testHall3.getId()).contains(_h.getId())));

    }

    @Test
    public void deleteTest() {
        expositionHallDAO.save(testExposition1, testHall1);
        expositionHallDAO.save(testExposition1, testHall2);
        expositionHallDAO.save(testExposition1, testHall3);

        final var halls1 = expositionHallDAO.get(testExposition1);
        assertEquals(3, halls1.size());
        assertTrue(halls1.stream()
                .allMatch(_h -> Set.of(testHall1.getId(), testHall2.getId(), testHall3.getId()).contains(_h.getId())));

        expositionHallDAO.delete(testExposition1, testHall2);

        final var halls2 = expositionHallDAO.get(testExposition1);
        assertEquals(2, halls2.size());
        assertTrue(halls2.stream()
                .allMatch(_h -> Set.of(testHall1.getId(), testHall3.getId()).contains(_h.getId())));

    }
}
