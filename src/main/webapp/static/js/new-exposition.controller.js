angular.module('expositions')
    .controller('NewExpositionController', ['$scope', '$log', '$window', 'baseUrl', 'expositionsService',
        function ($scope, $log, $window, baseUrl, expositionsService) {

            $scope.availableHalls = [];
            $scope.availableDays = [];
            $scope.exposition = {};
            $scope.schedule = [];

            init();

            function init() {

                expositionsService.getHalls()
                    .then((halls) => $scope.availableHalls = halls);

                $scope.exposition = {
                    topic: '',
                    dateFrom: new Date(),
                    dateTo: new Date(new Date().getTime() + (7 * 24 * 60 * 60 * 1000)),
                    schedule: {
                        weekday: $scope.availableDays[0],
                        timeFrom: new Date(1970, 0, 1, 9, 0, 0),
                        timeTo: new Date(1970, 0, 1, 18, 0, 0),
                    },
                    halls: [],
                    price: 0,
                };

                // populate days
                $scope.availableDays = getWeekdaysBetweenDates(localizedWeekdays,
                    getDaysBetweenDates($scope.exposition.dateFrom, $scope.exposition.dateTo));

                if ($scope.availableDays) {
                    $scope.exposition.schedule.weekday = $scope.availableDays[0];
                }
            }

            $scope.removeScheduleItem = function (weekday) {

                $scope.schedule = $scope.schedule.filter(_e => _e.weekday !== weekday);
                $scope.availableDays = sortDays([...$scope.availableDays, weekday]);

                if ($scope.availableDays) {
                    $scope.exposition.schedule.weekday = $scope.availableDays[0];
                }
            };

            $scope.addScheduleItem = function (day, timeFrom, timeTo) {

                if (day && timeFrom && timeTo) {
                    $scope.schedule = sortSchedule([...$scope.schedule,
                        {weekday: day, timeFrom, timeTo}]);

                    // remove from available days
                    $scope.availableDays = sortDays($scope.availableDays.filter(_d => _d.pos !== day.pos));

                    if ($scope.availableDays) {
                        $scope.exposition.schedule.weekday = $scope.availableDays[0];
                    }
                }
            };

            $scope.updateWeekdays = function () {
                if (!$scope.exposition.dateFrom || !$scope.exposition.dateTo) {
                    return;
                }

                const weekdaysSet = getDaysBetweenDates($scope.exposition.dateFrom, $scope.exposition.dateTo);

                // remove days not in range
                $scope.schedule = $scope.schedule.filter(_s => weekdaysSet.has(_s.weekday.pos));

                // remove already selected days
                $scope.schedule.forEach(_s => weekdaysSet.delete(_s.weekday.pos));

                $scope.availableDays = getWeekdaysBetweenDates(localizedWeekdays, weekdaysSet);

                if ($scope.availableDays) {
                    $scope.exposition.schedule.weekday = $scope.availableDays[0];
                }
            };

            $scope.createExposition = function () {
                $log.info(`create exposition: ${angular.toJson($scope.exposition)}`);
                $log.info(`base url: ${baseUrl}`);

                expositionsService.createExposition(prepareData($scope.exposition))
                    .then((result) => {
                        if (result.status === 'OK') {
                            $log.info('create result: ' + angular.toJson(result));
                            $window.location.replace(baseUrl);
                        } else if (result.status === 'ERROR') {
                            $log.error('create result: ' + angular.toJson(result));
                        }
                    }).catch(reason => $log.error(angular.toJson(reason)));
            };

            $scope.isDateRangeValid = function (dateFrom, dateTo) {
                if (!dateFrom || !dateTo) {
                    return false;
                }

                return dateFrom.getTime() < dateTo.getTime();
            };

            $scope.back = function () {
                $window.location.replace(baseUrl);

            };

            function sortDays(array) {
                const result = [...array];
                result.sort((_d1, _d2) => _d1.pos - _d2.pos);

                return result;
            }

            function sortSchedule(array) {
                const result = [...array];
                result.sort((_s1, _s2) => _s1.weekday.pos - _s2.weekday.pos);

                return result;
            }

            function getDaysBetweenDates(dateFrom, dateTo) {
                const set = new Set();

                if (!dateFrom || !dateTo || dateFrom.getTime() > dateTo.getTime()) {
                    return set;
                }
                const mDateFrom = moment(dateFrom).startOf('day');
                const mDateTo = moment(dateTo).startOf('day');

                while (mDateFrom.isSameOrBefore(mDateTo)) {
                    set.add(mDateFrom.isoWeekday());
                    mDateFrom.add(1, 'days');
                }

                return set;
            }

            function getWeekdaysBetweenDates(localizedDays, daySet) {
                const result = [];
                daySet.forEach(_v => result.push(localizedDays[_v - 1]));

                return sortDays(result);
            }

            function prepareData(exposition) {
                const result = {};

                if (!exposition) {
                    return result;
                }

                result.topic = exposition.topic;
                result.dateFrom = moment(exposition.dateFrom).format('YYYY-MM-DD');
                result.dateTo = moment(exposition.dateTo).format('YYYY-MM-DD');
                result.halls = exposition.halls;
                result.price = exposition.price;
                result.schedule = $scope.schedule.map((_s) => ({
                    weekday: _s.weekday.code,
                    timeFrom: moment(_s.timeFrom).format('HH:mm'),
                    timeTo: moment(_s.timeTo).format('HH:mm'),
                }));

                return result;
            }
        }]);
