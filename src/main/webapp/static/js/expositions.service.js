angular.module('expositions')
    .factory('expositionsService', ['$http', '$q', '$window', 'baseUrl',
        function expositionsService($http, $q, $window, baseUrl) {

            function get(url, extractFn) {
                const deferred = $q.defer();

                $http.get(url)
                    .then(result => deferred.resolve(extractFn(result)), deferred.reject);

                return deferred.promise;
            }

            function get2(url, params) {
                const deferred = $q.defer();

                $http.get(url, {params})
                    .then(result => {
                        if (result.data.status === 'OK') {
                            deferred.resolve(result.data.data);
                        } else if (result.data.status === 'ERROR') {
                            deferred.reject(result.data.errorMessage);
                        }
                    }).catch(deferred.reject);

                return deferred.promise;
            }

            function getHalls() {
                return get(`${baseUrl}/api/hall`, result => result.data.data);
            }

            function getSoldTickets(expositionId) {
                return get2(`${baseUrl}/api/ticket/sold/${expositionId}`);
            }

            function buyTicket(expositionId, date) {
                const body = {
                    expositionId, date: moment(date).format('YYYY-MM-DD')
                };
                const deferred = $q.defer();

                $http.post(`${baseUrl}/ticket?action=buyTicket`, body)
                    .then((result) => {
                        if (result.data.status === 'OK') {
                            deferred.resolve(result.data.data);
                        } else if (result.data.status === 'ERROR') {
                            deferred.reject(result.data.errorMessage);
                        }
                    }, deferred.reject);

                return deferred.promise;
            }

            function getUserInfo() {
                return get2(`${baseUrl}/api/user`);
            }

            function createExposition(exposition) {
                const deferred = $q.defer();

                $http.post(`${baseUrl}/exposition?action=create`, exposition)
                    .then((result) => deferred.resolve(result.data), deferred.reject);

                return deferred.promise;
            }

            function getExposition(expositionId) {
                return get(`${baseUrl}/api/exposition/${expositionId}`, result => result.data.data);
            }

            function getExpositions(filter) {
                return get2(`${baseUrl}/api/exposition`, filter);
            }

            function removeExposition(expositionId) {
                return get2(`${baseUrl}/exposition?action=remove&id=${expositionId}`);
            }

            function getParameter(paramName) {
                const params = new URLSearchParams($window.location.search);
                return params.get(paramName);
            }

            function login(username, password) {
                const deferred = $q.defer();

                $http.post(`${baseUrl}/api/login`, {username, password})
                    .then(result => {
                        if (result.data.status === 'OK') {
                            deferred.resolve(result.data.data);
                        } else if (result.data.status === 'ERROR') {
                            deferred.reject(result.data.errorMessage);
                        }
                    }).catch(deferred.reject);

                return deferred.promise;
            }

            function register(data) {
                const deferred = $q.defer();

                $http.post(`${baseUrl}/api/register`, data)
                    .then(result => {
                        if (result.data.status === 'OK') {
                            deferred.resolve(result.data.data);
                        } else if (result.data.status === 'ERROR') {
                            deferred.reject(result.data.errorMessage);
                        }
                    })
                    .catch(deferred.reject);

                return deferred.promise;
            }

            return {
                getHalls,
                createExposition, getExposition, getExpositions, removeExposition,
                getSoldTickets, buyTicket,
                getUserInfo, login, register,
                getParameter,
            };
        }]);
