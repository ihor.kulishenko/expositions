angular.module('expositions')
    .controller('LoginController', ['$scope', '$window', 'baseUrl', 'expositionsService',
        function ($scope, $window, baseUrl, expositionsService) {
            $scope.login = '';
            $scope.password = '';
            $scope.errorMessage = '';

            $scope.userLogin = function () {
                expositionsService.login($scope.login, $scope.password)
                    .then(() => $window.location.replace(baseUrl))
                    .catch(reason => {
                        $scope.errorMessage = reason;
                        $scope.password = '';
                    });
            };

            $scope.back = function () {
                $window.location.replace(baseUrl);
            };
        }]);
