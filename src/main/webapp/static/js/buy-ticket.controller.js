angular.module('expositions')
    .controller('BuyTicketModalController', ['$scope', '$log', '$window', 'baseUrl', 'expositionsService',
        function ($scope, $log, $window, baseUrl, expositionsService) {
            $scope.buyDate = $scope.exposition.dateFrom;

            const buyTicketDialog = new bootstrap.Modal(document.getElementById('buyTicketModal'), {});

            $scope.buyTicket = function () {
                buyTicketDialog.show();

                expositionsService.buyTicket($scope.exposition.id, $scope.buyDate)
                    .then(() => $scope.user.isAdmin
                        ? expositionsService.getSoldTickets($scope.exposition.id)
                        : Promise.resolve($scope.exposition.soldTickets))
                    .then((soldTickets) => $scope.exposition.soldTickets = soldTickets)
                    .catch($log.error)
                    .finally(() => {
                        buyTicketDialog.hide();
                        $scope.buyDate = $scope.exposition.dateFrom;
                    });
            };
        }]);
