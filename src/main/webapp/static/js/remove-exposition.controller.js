angular.module('expositions')
    .controller('RemoveExpositionDialogController', ['$scope', '$log', '$window', 'baseUrl', 'expositionsService',
        function ($scope, $log, $window, baseUrl, expositionsService) {
            $scope.removeExposition = function () {
                const expositionId = expositionsService.getParameter('id');

                $log.info(`remove expositionId: ${expositionId}`);

                expositionsService.removeExposition(expositionId)
                    .then(() => $window.location.replace(baseUrl))
                    .catch($log.error);
            };
        }]);
