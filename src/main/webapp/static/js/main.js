window.onload = main;

function main() {
    const FILTER_BUTTON_ELEMENT = 'filterButton';
    const FILTER_DATE_FROM_ELEMENT = 'filterDateFrom';
    const FILTER_DATE_TO_ELEMENT = 'filterDateTo';

    const COLUMN_HEADER_TOPIC = 'columnTopic';
    const COLUMN_HEADER_DATE_FROM = 'columnDateFrom';
    const COLUMN_HEADER_DATE_TO = 'columnDateTo';
    const COLUMN_HEADER_PRICE = 'columnPrice';

    const LANGUAGE_SELECT_ELEMENT = 'langSelect';

    const store = {
        'filter': {},
        'sort': {
            'column': '',
            'order': true // true - ascending, false - descending
        },
        'lang': ''
    };

    // register handler for filter
    const filterButtonElement = document.getElementById(FILTER_BUTTON_ELEMENT);
    filterButtonElement.onclick = filterButtonHandler;

    const filterDateFrom = document.getElementById(FILTER_DATE_FROM_ELEMENT);
    const filterDateTo = document.getElementById(FILTER_DATE_TO_ELEMENT);

    // register handler for table headers
    [COLUMN_HEADER_TOPIC, COLUMN_HEADER_DATE_FROM, COLUMN_HEADER_DATE_TO, COLUMN_HEADER_PRICE].forEach(name => {
        document.getElementById(name).onclick = sortButtonHandler;
    });

    // register handler for paginator
    document.querySelectorAll(':scope .pagination .page-item')
        .forEach(_e => {
            const elementClass = _e.getAttribute('class');

            if (!elementClass.includes('disabled') && !elementClass.includes('active')) {
                _e.onclick = paginatorHandler;
            }
        });

    // register handler for language selector
    const langSelectElement = document.getElementById(LANGUAGE_SELECT_ELEMENT);
    langSelectElement.onchange = langSelectHandler;

    initStore();

    function filterButtonHandler() {
        updateFilter();
        redrawPage();
    }

    function sortButtonHandler(event) {
        const column = event.target.dataset.column;
        updateSort(column);

        redrawPage();
    }

    function paginatorHandler(event) {
        const page = event.currentTarget.dataset.page;
        updatePage(page);

        redrawPage();
    }

    function langSelectHandler(event) {
        updateLang(event.target.value);
        redrawPage();
    }

    function redrawPage() {
        window.location.href = makeUrlFromStore();
    }

    function initStore() {
        store.filter.dateFrom = filterDateFrom.value;
        store.filter.dateTo = filterDateTo.value;

        store.sort.column = sortColumn;
        store.sort.order = sortOrder !== 'DSC';
    }

    function updateFilter() {
        store.filter.dateFrom = filterDateFrom.value;
        store.filter.dateTo = filterDateTo.value;
    }

    function updateSort(column) {
        if (store.sort.column !== column) {
            store.sort.column = column;
            store.sort.order = true;
        } else {
            store.sort.order = !store.sort.order;
        }
    }

    function updatePage(page) {
        store.page = page;
    }

    function updateLang(lang) {
        store.lang = lang;
    }

    function makeUrlFromStore() {
        const getSortOrder = (order) => order ? 'ASC' : 'DSC';

        const parameters = [
            (store.filter.dateFrom ? `filterDateFrom=${store.filter.dateFrom}` : ''),
            (store.filter.dateTo ? `filterDateTo=${store.filter.dateTo}` : ''),
            (store.sort.column ? `sortColumn=${store.sort.column}&sortOrder=${getSortOrder(store.sort.order)}` : ''),
            (store.page ? `page=${store.page}` : ''),
            (store.lang ? `lang=${store.lang}` : '')
        ].filter(_p => _p.length > 0).join('&');

        return baseUrl + '/?' + parameters;
    }
}
