angular.module('expositions')
    .controller('ViewExpositionController', ['$scope', '$log', '$window', 'baseUrl', 'expositionsService',
        function ($scope, $log, $window, baseUrl, expositionsService) {

            $scope.exposition = {soldTickets: 0};
            $scope.user = {
                isAdmin: false,
                isAuthorized: false
            };

            $scope.back = function () {
                $window.location.replace(baseUrl);
            };

            init();

            function init() {

                const expositionId = expositionsService.getParameter('id');

                expositionsService.getExposition(expositionId)
                    .then(exposition => {

                        $scope.exposition.id = exposition.id;
                        $scope.exposition.topic = exposition.topic;
                        $scope.exposition.dateFrom = toDate(exposition.dateFrom);
                        $scope.exposition.dateTo = toDate(exposition.dateTo);
                        $scope.exposition.schedule = toSchedule(exposition.schedule);
                        $scope.exposition.halls = exposition.halls;
                        $scope.exposition.price = exposition.price;
                    })
                    .catch($log.error);

                expositionsService.getUserInfo()
                    .then(userInfo => {
                        $scope.user = userInfo;
                        $scope.user.isAuthorized = true;

                        return $scope.user.isAdmin;
                    })
                    .then((isAdmin) => isAdmin ? expositionsService.getSoldTickets(expositionId) : Promise.resolve(0))
                    .then(soldTickets => $scope.exposition.soldTickets = soldTickets)
                    .catch($log.error);

            }

            function toDate(array) {
                const result = new Date();
                result.setMonth(array[1] - 1, array[2]);
                result.setFullYear(array[0]);

                return result;
            }

            function toTime(array) {
                return new Date(1970, 0, 1, array[0], array[1]);
            }

            function toSchedule(array) {
                return array.map(_e => ({
                    weekday: _e.lcWeekday,
                    timeFrom: toTime(_e.timeFrom),
                    timeTo: toTime(_e.timeTo),
                }));
            }
        }]);
