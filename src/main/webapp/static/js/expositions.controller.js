angular.module('expositions')
    .controller('ExpositionsController', ['$scope', '$window', '$log', '$filter', 'baseUrl', 'expositionsService',
        function ($scope, $window, $log, $filter, baseUrl, expositionsService) {

            $scope.login = function () {
                $window.location.href = `${baseUrl}/template/login`;
            };

            $scope.logout = function () {
                $window.location.href = `${baseUrl}/template/logout`;
            };

            $scope.register = function () {
                $window.location.href = `${baseUrl}/template/register`;
            };

            $scope.addExposition = function () {
                $window.location.href = `${baseUrl}/template/newExposition`;
            };

            $scope.filterExposition = function () {
                expositionsService.getExpositions(getFilter())
                    .then(processExpositions)
                    .catch($log.error);
            };

            $scope.sortTableColumn = function (columnName) {
                if (columnName === $scope.sort.column) {
                    $scope.sort.ascending = !$scope.sort.ascending;
                } else {
                    $scope.sort.column = columnName;
                    $scope.sort.ascending = true;
                }

                $log.info(`sort: ${angular.toJson($scope.sort)}`);

                $scope.filterExposition();
            };

            $scope.langChanged = function () {
                $log.info(`current lang is: ${$scope.currentLanguage}`);
            };

            $scope.getExpositionLink = function (expositionId) {
                return `${baseUrl}/template/viewExposition?id=${expositionId}`;
            };

            $scope.changePage = function (page) {
                $log.info(`info page changed to ${page}`);

                $scope.pager.page = page;
                $scope.filterExposition();
            }

            init();

            function init() {
                $scope.user = {};

                $scope.filter = {
                    dateFrom: null,
                    dateTo: null,
                };

                $scope.sort = {
                    column: null,
                    ascending: true
                };

                $scope.pager = {
                    page: 1
                };

                $scope.expositions = [
                    {id: 1, topic: 'exposition topic', start: new Date(), finish: new Date(), price: 100}];

                $scope.availableLangs = ['en', 'uk'];
                $scope.currentLanguage = $scope.availableLangs[0];

                expositionsService.getUserInfo()
                    .then((userInfo) => {
                        $scope.user = userInfo;
                        $scope.user.isAuthorized = true;
                    })
                    .catch($log.error);

                $scope.filterExposition();
            }

            function getFilter() {
                return {
                    dateFrom: $filter('date')($scope.filter.dateFrom, 'yyyy-MM-dd'),
                    dateTo: $filter('date')($scope.filter.dateTo, 'yyyy-MM-dd'),
                    column: $scope.sort.column,
                    order: $scope.sort.ascending ? 'asc' : 'desc',
                    page: $scope.pager.page,
                };
            }

            function processExpositions(data) {
                $scope.expositions = data.expositions.map(_ex => ({
                    ..._ex,
                    dateFrom: convertDate(_ex.dateFrom),
                    dateTo: convertDate(_ex.dateTo),
                }));

                ifExists(data.sortColumn, x => $scope.sort.column = x);
                ifExists(data.sortOrder, x => $scope.sort.ascending = x === 'asc');
                ifExists(data.dateFrom, x => $scope.filter.dateFrom = convertDate(x));
                ifExists(data.dateTo, x => convertDate(x));
                ifExists(data.currentPage, x => $scope.pager.page = x);
                ifExists(data.totalPages, x => $scope.pager.total = x);
                ifExists(data.elementsPerPage, x => $scope.pager.epp = x);
            }

            function convertDate(array) {
                return new Date(array[0], array[1] - 1, array[2]);
            }

            function ifExists(value, fn) {
                if (value) {
                    fn(value);
                }
            }
        }]);
