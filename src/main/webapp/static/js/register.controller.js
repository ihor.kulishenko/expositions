angular.module('expositions')
    .controller('RegisterController', ['$scope', '$window', 'baseUrl', 'expositionsService',
        function ($scope, $window, baseUrl, expositionsService) {
            $scope.errorMessage = '';
            $scope.firstname = '';
            $scope.lastname = '';
            $scope.login = '';
            $scope.email = '';
            $scope.password = '';
            $scope.password2 = '';

            $scope.userRegister = function () {
                expositionsService.register({
                    firstname: $scope.firstname,
                    lastname: $scope.lastname,
                    login: $scope.login,
                    email: $scope.email,
                    password: $scope.password,
                    password2: $scope.password2,
                }).then(() => $window.location.replace(baseUrl))
                    .catch(reason => {
                        $scope.errorMessage = reason;
                        $scope.password = '';
                        $scope.password2 = '';
                    });
            };

            $scope.back = function () {
                $window.location.replace(baseUrl);
            };
        }]);
