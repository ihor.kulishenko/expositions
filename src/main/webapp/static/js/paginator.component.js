angular.module('expositions')
    .component('paginator', {
        templateUrl: `${baseUrl}/template/paginator`,
        controller: PaginatorController,
        bindings: {
            currentPage: '<',
            totalPages: '<',
            onPageChange: '&'
        },
    });

function PaginatorController() {
    const ctrl = this;

    ctrl.isFirstPage = function () {
        return ctrl.currentPage === 1;
    };

    ctrl.isLastPage = function () {
        return ctrl.currentPage === ctrl.totalPages;
    };

    ctrl.previousPage = function () {
        return ctrl.currentPage > 1 ? ctrl.currentPage - 1 : 1;
    };

    ctrl.hasPreviousPage = function () {
        return ctrl.currentPage > 1;
    };

    ctrl.nextPage = function () {
        return ctrl.currentPage > ctrl.totalPages ? ctrl.totalPages : ctrl.currentPage + 1;
    };

    ctrl.hasNextPage = function () {
        return ctrl.currentPage < ctrl.totalPages;
    };

    ctrl.changePage = function (page) {
        ctrl.onPageChange({page});
    };
}