<link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}/favicon.ico"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap-icons.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bootstrap.min.css" type="text/css">
<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js" lang="javascript"></script>
