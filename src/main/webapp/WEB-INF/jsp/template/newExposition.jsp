<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--@elvariable id="currentLanguage" type="java.lang.String"--%>

<fmt:setLocale value="${currentLanguage}"/>
<fmt:setBundle basename="messages"/>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><fmt:message key="newExp.title"/></title>
    <jsp:include page="../header.jsp"/>
</head>
<body ng-app="expositions">
<div class="container">
    <div class="row vh-100" id="rootRow">
        <div class="col-7 border border-2 rounded mx-auto my-auto bg-light">
            <form novalidate ng-controller="NewExpositionController" name="form">
                <div class="row p-2 justify-content-center">

                    <div class="row mt-3">
                        <div class="col">
                            <p class="fs-5 mb-0 -2">
                                <label for="topicField" class="form-label">
                                    <fmt:message key="newExp.label.topic"/>
                                </label>
                            </p>
                        </div>
                        <div class="col-11">
                           <textarea name="topic" id="topicField" class="form-control"
                                     ng-model="exposition.topic"
                                     required
                                     ng-class="{'is-invalid': form.topic.$touched && form.topic.$invalid,
                                     'is-valid': form.topic.$touched && form.topic.$valid}"
                                     placeholder="<fmt:message key="newExp.placeholder.topic"/>"></textarea>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-6">
                            <%-- Exposition date  --%>
                            <div class="row">
                                <div class="col-auto">
                                    <p class="fs-5 mb-0"><fmt:message key="newExp.label.date"/>:</p>
                                </div>
                            </div>

                            <div class="row mt-2 justify-content-start align-items-baseline">
                                <div class="col-2 pe-0">
                                    <label for="dateFromField" class="form-label">
                                        <fmt:message key="newExp.label.dateFrom"/>:
                                    </label>
                                </div>
                                <div class="col-8">
                                    <input type="date" name="dateFrom" id="dateFromField" class="form-control"
                                           required
                                           ng-model="exposition.dateFrom"
                                           ng-change="updateWeekdays()"
                                           ng-class="{'is-valid': form.dateFrom.$valid && form.dateFrom.$touched,
                                            'is-invalid': form.dateFrom.$invalid && form.dateFrom.$touched}">
                                </div>
                            </div>
                            <div class="row mt-2 justify-content-start align-items-baseline-">
                                <div class="col-2 pe-0">
                                    <label for="dateToField" class="form-label">
                                        <fmt:message key="newExp.label.dateTo"/>:
                                    </label>
                                </div>
                                <div class="col-8">
                                    <input type="date" name="dateTo" id="dateToField" class="form-control"
                                           required
                                           ng-model="exposition.dateTo"
                                           ng-change="updateWeekdays()"
                                           ng-class="{'is-valid': form.dateTo.$valid && form.dateTo.$touched,
                                            'is-invalid': form.dateTo.$invalid && form.dateTo.$touched}">
                                </div>
                            </div>


                        </div>
                        <div class="col-6">
                            <%-- Halls  --%>
                            <div class="row">
                                <div class="col-auto">
                                    <p class="fs-5 mb-0">
                                        <label for="hallsField">
                                            <fmt:message key="newExp.label.halls"/>:
                                        </label>
                                    </p>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-10">
                                    <select name="halls" id="hallsField" size="{{ availableHalls.length }}" multiple
                                            ng-model="exposition.halls"
                                            ng-class="{'is-valid': form.halls.$touched && form.halls.$valid,
                                             'is-invalid': form.halls.$touched && form.halls.$invalid}"
                                            class="form-control" required>
                                        <option ng-repeat="hall in availableHalls" ng-value="hall.id">{{ hall.name }}
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-6">
                            <%-- schedule --%>
                            <div class="row">
                                <div class="col-auto">
                                    <p class="fs-5 mb-0">
                                        <fmt:message key="newExp.label.schedule"/>
                                    </p>
                                </div>
                            </div>
                            <div class="row mt-2 justify-content-start">
                                <div class="col-8">
                                    <select class="form-select" aria-label="Select Weekday" id="weekdaySelect"
                                            name="weekday" ng-model="exposition.schedule.weekday"
                                            ng-options="day as day.value for day in availableDays">
                                    </select>
                                </div>

                                <div class="col-3">
                                    <button type="button" class="btn btn-outline-dark" id="addScheduleButton"
                                            ng-disabled="form.timeFrom.$invalid || form.timeTo.$invalid || !availableDays.length
                                            || !isDateRangeValid(exposition.schedule.timeFrom, exposition.schedule.timeTo)"

                                            ng-click="addScheduleItem(exposition.schedule.weekday,
                                            exposition.schedule.timeFrom, exposition.schedule.timeTo)">
                                        <fmt:message key="newExp.button.addSchedule"/>
                                    </button>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-2 pe-0">
                                    <p>
                                        <label class="form-label" for="timeFromField">From</label>
                                    </p>
                                </div>
                                <div class="col-6">
                                    <input type="time" name="timeFrom" id="timeFromField" class="form-control"
                                           ng-model="exposition.schedule.timeFrom"
                                           ng-class="{'is-invalid': form.timeFrom.$invalid && form.timeFrom.$touched}"
                                           required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    <p>
                                        <label for="timeToField" class="form-label">To</label>
                                    </p>
                                </div>
                                <div class="col-6">
                                    <input type="time" name="timeTo" id="timeToField" class="form-control"
                                           ng-model="exposition.schedule.timeTo"
                                           ng-class="{'is-invalid': form.timeTo.$invalid && form.timeTo.$touched}"
                                           required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9">
                                    <ul class="list-group" id="scheduleField">
                                        <li class="list-group-item d-flex justify-content-between align-items-center"
                                            ng-repeat="scheduleItem in schedule">

                                            <span class="flex-grow-1">{{ scheduleItem.weekday.value }}</span>

                                            <span class="flex-shrink-0 pe-4">
                                                {{scheduleItem.timeFrom | date:'HH:mm'}}-{{scheduleItem.timeTo | date:'HH:mm'}}
                                            </span>
                                            <button type="button" class="badge bg-danger"
                                                    ng-click="removeScheduleItem(scheduleItem.weekday)">X
                                            </button>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <%-- price --%>
                            <div class="row">
                                <%-- ticket price --%>
                                <div class="col-auto">
                                    <p class="fs-5 mb-0">
                                        <label for="priceField"><fmt:message key="newExp.label.price"/></label>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-auto">
                                    <input type="number" id="priceField" name="price" class="form-control"
                                           required
                                           ng-model="exposition.price"
                                           ng-class="{'is-invalid': form.price.$touched && form.price.$invalid,
                                           'is-valid': form.price.$touched && form.price.$valid}"
                                           ng-min="0">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-5 mb-2">
                        <%-- buttons --%>
                        <div class="col-12">
                            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                <button class="btn btn-primary" type="button" id="createExpositionButton"
                                        ng-disabled="form.$invalid || !schedule.length"
                                        ng-click="createExposition()"
                                >
                                    <fmt:message key="newExp.button.createExp"/>
                                </button>
                                <button class="btn btn-secondary ms-3 " type="button" ng-click="back()">
                                    <fmt:message key="newExp.button.cancel"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    const baseUrl = '${pageContext.request.contextPath}';
</script>
<script src="${pageContext.request.contextPath}/static/js/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/moment.js"></script>
<script src="${pageContext.request.contextPath}/static/js/expositions.module.js"></script>
<script src="${pageContext.request.contextPath}/static/js/expositions.service.js"></script>
<script src="${pageContext.request.contextPath}/static/js/new-exposition.controller.js"></script>
</body>
</html>
