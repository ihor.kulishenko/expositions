<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--@elvariable id="currentLanguage" type="java.lang.String"--%>

<fmt:setLocale value="${currentLanguage}"/>
<fmt:setBundle basename="messages"/>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <fmt:message key="login.title"/>
    </title>
    <jsp:include page="../header.jsp"/>

</head>
<body>
<div class="container" ng-app="expositions">
    <div class="row d-flex vh-100 align-items-center justify-content-center">
        <div class="col-3">
            <form class="row border bg-light" name="loginForm" ng-controller="LoginController" novalidate>

                <div class="col-12 d-flex justify-content-center border-bottom">
                    <h4 class="form-label"><fmt:message key="login.label.login"/></h4>
                </div>

                <div ng-if="errorMessage" class="col-12 d-flex justify-content-center">
                    <span class="form-text text-danger">{{ errorMessage }}</span>
                </div>

                <div class="col-12 gy-3">
                    <h5 class="form-label">
                        <label for="usernameField"><fmt:message key="login.label.username"/></label>
                    </h5>
                </div>

                <div class="col-12">
                    <input type="text" autofocus class="form-control" name="username" id="usernameField"
                           ng-model="login" required>
                </div>

                <div class="col-12 gy-3">
                    <h5 class="form-label">
                        <label for="passwordField"><fmt:message key="login.label.password"/></label>
                    </h5>
                </div>

                <div class="col-12">
                    <input type="password" class="form-control" name="password" ng-model="password" id="passwordField"
                           required>
                </div>

                <div class="col-12 d-flex justify-content-end border-top mt-2 pt-2 pb-2">
                    <button type="submit" class="btn btn-primary pt-2" ng-disabled="loginForm.$invalid"
                            ng-click="userLogin()">
                        <fmt:message key="login.button.login"/>
                    </button>

                    <button class="btn btn-secondary ms-3 " type="button" ng-click="back()">
                        <fmt:message key="login.button.back"/>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    const baseUrl = '${pageContext.request.contextPath}';
</script>

<script src="${pageContext.request.contextPath}/static/js/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/moment.js"></script>

<script src="${pageContext.request.contextPath}/static/js/expositions.module.js"></script>
<script src="${pageContext.request.contextPath}/static/js/expositions.service.js"></script>
<script src="${pageContext.request.contextPath}/static/js/login.controller.js"></script>
</body>
</html>
