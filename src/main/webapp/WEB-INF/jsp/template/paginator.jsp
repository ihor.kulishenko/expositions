<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${requestScope.get('currentLanguage')}"/>
<fmt:setBundle basename="messages"/>

<ul class="pagination justify-content-center">

    <li class="page-item "
        ng-class="{'disabled': $ctrl.isFirstPage()}"
        ng-click="$ctrl.changePage(1)"
        data-bs-toggle="tooltip"
        title="<fmt:message key="paginator.firstPage.tooltip"/>"
        data-page="1">
        <a class="page-link" role="button">
            <i class="bi bi-chevron-bar-left"></i>
        </a>
    </li>

    <li class="page-item " data-bs-toggle="tooltip"
        ng-class="{'disabled': $ctrl.isFirstPage()}"
        ng-click="$ctrl.changePage($ctrl.previousPage())"
        title="<fmt:message key="paginator.previousPage.tooltip"/>"
        data-page="{{ $ctrl.previousPage() }}">
        <a class="page-link">
            <i class="bi bi-chevron-left"></i>
        </a>
    </li>

    <li ng-if="$ctrl.hasPreviousPage()"
        ng-click="$ctrl.changePage($ctrl.previousPage())"
        class="page-item" data-page="{{ $ctrl.previousPage() }}">
        <a class="page-link" role="button">{{ $ctrl.previousPage() }}</a>
    </li>

    <li class="page-item active" data-page="{{ $ctrl.currentPage }}">
        <a class="page-link" role="button">{{ $ctrl.currentPage }}</a>
    </li>

    <li ng-if="$ctrl.hasNextPage()"
        ng-click="$ctrl.changePage($ctrl.nextPage())"
        class="page-item" data-page="{{ $ctrl.nextPage() }}">
        <a class="page-link" role="button">{{ $ctrl.nextPage() }}</a>
    </li>

    <li class="page-item"
        ng-class="{'disabled': $ctrl.isLastPage()}"
        ng-click="$ctrl.changePage($ctrl.nextPage())"
        data-bs-toggle="tooltip"
        title="<fmt:message key="paginator.nextPage.tooltip"/>"
        data-page="{{ $ctrl.nextPage() }}">
        <a class="page-link" role="button">
            <i class="bi bi-chevron-right"></i>
        </a>
    </li>

    <li class="page-item"
        ng-class="{'disabled': $ctrl.isLastPage()}"
        ng-click="$ctrl.changePage($ctrl.totalPages)"
        data-bs-toggle="tooltip"
        title="<fmt:message key="paginator.lastPage.tooltip"/>"
        data-page="{{ $ctrl.totalPages }}">
        <a class="page-link" role="button">
            <i class="bi bi-chevron-bar-right"></i>
        </a>
    </li>
</ul>
