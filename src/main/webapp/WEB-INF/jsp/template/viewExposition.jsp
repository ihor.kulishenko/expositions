<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--@elvariable id="currentLanguage" type="java.lang.String"--%>

<fmt:setLocale value="${currentLanguage}"/>
<fmt:setBundle basename="messages"/>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><fmt:message key="viewExp.title"/></title>

    <jsp:include page="../header.jsp"/>
</head>
<body ng-app="expositions">

<div class="container" id="rootRow" ng-controller="ViewExpositionController">
    <div class="row vh-100">
        <div class="col-7 border border-2 rounded mx-auto my-auto bg-light">
            <%-- Header --%>
            <div class="row p-3 border-bottom border-1">
                <div class="col">
                    <p class="m-0 text-center fs-3">
                        <fmt:message key="viewExp.title"/>
                    </p>
                </div>
            </div>

            <%-- Body --%>
            <div class="row mt-3 justify-content-center ">

                <%-- topic --%>
                <div class="row flex-column">
                    <div class="col">
                        <p class="fs-5 mb-0">
                            <fmt:message key="viewExp.label.topic"/>
                        </p>
                    </div>
                    <div class="col-11">
                        <p class="border rounded p-2 bg-white">
                            {{ exposition.topic }}
                        </p>
                    </div>
                </div>

                <%-- Date --%>
                <div class="row flex-column">
                    <div class="col">
                        <p class="fs-5 mb-0">
                            <fmt:message key="viewExp.label.date"/>
                        </p>
                    </div>
                    <div class="col-6">
                        <p class="border rounded p-2 bg-white">
                            <span class="fw-bold">
                                <fmt:message key="viewExp.label.dateFrom"/>:
                            </span>
                            <span class="ms-1">{{ exposition.dateFrom | date:'dd.MM.yyyy' }}</span>

                            <span class="ms-3 fw-bold">
                                <fmt:message key="viewExp.label.dateTo"/>:
                            </span>
                            <span class="ms-1">{{ exposition.dateTo | date:'dd.MM.yyyy'}}</span>
                        </p>
                    </div>
                </div>

                <%-- Schedule --%>
                <div class="row flex-column">
                    <div class="col">
                        <p class="fs-5 mb-0">
                            <fmt:message key="viewExp.label.schedule"/>
                        </p>
                    </div>
                    <div class="col d-flex p-2 align-items-stretch border rounded bg-white" style="gap: 12px;">
                        <p class="m-0 text-center d-flex flex-column"
                           ng-repeat="scheduleItem in exposition.schedule">
                            <span>{{ scheduleItem.weekday }}</span>
                            <span>
                                {{scheduleItem.timeFrom | date:'HH:mm'}}-{{scheduleItem.timeTo | date:'HH:mm'}}
                            </span>
                        </p>

                    </div>
                </div>

                <div class="row mt-3">
                    <%-- Halls --%>
                    <div class="col-6">
                        <div class="col">
                            <p class="fs-5 mb-0">
                                <fmt:message key="viewExp.label.halls"/>
                            </p>
                        </div>
                        <div class="col d-flex flex-column border rounded p-2 bg-white">
                            <p class="m-0" ng-repeat="hall in exposition.halls">
                                {{ hall.name }}
                            </p>
                        </div>
                    </div>

                    <%-- Price --%>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-6">
                                <div class="col p-0">
                                    <p class="fs-5 mb-0">
                                        <fmt:message key="viewExp.label.price"/>
                                    </p>
                                </div>
                                <div class="border rounded p-2 bg-white">
                                    <p class="m-0">{{ exposition.price | currency:'':2 }}</p>
                                </div>
                            </div>
                            <div class="col-6" ng-if="user.isAdmin">
                                <div class="row d-flex flex-column">
                                    <div class="col p-0">
                                        <p class="fs-5 mb-0">
                                            <fmt:message key="viewExp.label.soldTickets"/>
                                        </p>
                                    </div>
                                    <div class="border rounded p-2 bg-white">
                                        <p class="m-0" id="soldTickets">{{ exposition.soldTickets }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <%-- Footer --%>
            <div class="row p-3 mt-3 border-top border-1">
                <div class="col d-flex justify-content-end">

                    <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                            data-bs-target="#removeConfirmationModal" id="removeButton"
                            ng-if="user.isAdmin">
                        <fmt:message key="viewExp.button.removeExp"/>
                    </button>

                    <button type="button" class="btn btn-primary ms-3" data-bs-toggle="modal"
                            data-bs-target="#buyTicketModal"
                            ng-if="user.isAuthorized">
                        <fmt:message key="viewExp.button.buyTicket"/>
                    </button>

                    <button class="btn btn-secondary ms-3 " type="button" ng-click="back()">
                        <fmt:message key="viewExp.button.back"/>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <%---------- Buy ticket Modal start ----------%>
    <div class="modal fade" id="buyTicketModal" tabindex="-1" aria-labelledby="buyTicketModalLabel" aria-hidden="true"
         ng-controller="BuyTicketModalController">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title w-100 text-center" id="buyTicketModalLabel">
                        <fmt:message key="buyTicketModal.title"/>
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form name="buyTicketForm">
                    <div class="modal-body">

                        <div class="row mb-3">
                            <label for="ticketDate" class="col-sm-4 col-form-label text-end">
                                <fmt:message key="buyTicketModal.label.chooseDate"/>:
                            </label>
                            <div class="col-sm-7">
                                <input type="date" class="form-control" id="ticketDate" name="ticketDate" required
                                       ng-model="buyDate"
                                       ng-class="{'is-invalid': buyTicketForm.ticketDate.$touched
                                            && buyTicketForm.ticketDate.$invalid,
                                        'is-valid': buyTicketForm.ticketDate.$touched
                                            && buyTicketForm.ticketDate.$valid}"
                                       min="{{exposition.dateFrom | date:'yyyy-MM-dd'}}"
                                       max="{{exposition.dateTo | date:'yyyy-MM-dd'}}">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="ticketPrice" class="col-sm-4 col-form-label text-end">
                                <fmt:message key="buyTicketModal.label.ticketPrice"/>:
                            </label>
                            <div class="col-sm-7">
                                <p id="ticketPrice" class="form-control-plaintext">
                                    {{ exposition.price | currency:'':2 }}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center" id="errorMessage">

                            </div>
                        </div>
                        <input type="hidden" id="expositionId" name="expositionId" ng-value="exposition.id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="buyButton" ng-click="buyTicket()"
                                ng-disabled="buyTicketForm.$invalid">
                            <fmt:message key="buyTicketModal.button.buy"/>
                        </button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                            <fmt:message key="buyTicketModal.button.cancel"/>
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <%---------- Buy ticket Modal end ----------%>

</div>


<%---------- Confirmation modal start ----------%>
<div class="modal fade" id="removeConfirmationModal" tabindex="-1" aria-labelledby="exampleModalLabel"
     aria-hidden="true" ng-controller="RemoveExpositionDialogController">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title w-100 text-center" id="exampleModalLabel">Confirmation</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <span>
                    <fmt:message key="deleteExpModal.message.confirm"/>
                </span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                    <fmt:message key="deleteExpModal.button.cancel"/>
                </button>
                <button type="button" class="btn btn-danger" id="removeButtonConfirm" ng-click="removeExposition()">
                    <fmt:message key="deleteExpModal.button.delete"/>
                </button>
            </div>
        </div>
    </div>
</div>
<%---------- Confirmation modal end ----------%>
<script>
    const baseUrl = '${pageContext.request.contextPath}';
</script>

<script src="${pageContext.request.contextPath}/static/js/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/moment.js"></script>

<script src="${pageContext.request.contextPath}/static/js/expositions.module.js"></script>
<script src="${pageContext.request.contextPath}/static/js/expositions.service.js"></script>
<script src="${pageContext.request.contextPath}/static/js/remove-exposition.controller.js"></script>
<script src="${pageContext.request.contextPath}/static/js/buy-ticket.controller.js"></script>
<script src="${pageContext.request.contextPath}/static/js/view-exposition.controller.js"></script>
</body>
</html>
