<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--@elvariable id="currentLanguage" type="java.lang.String"--%>

<fmt:setLocale value="${currentLanguage}"/>
<fmt:setBundle basename="messages"/>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <fmt:message key="register.title"/>
    </title>
    <jsp:include page="../header.jsp"/>

</head>
<body>
<div class="container" ng-app="expositions">
    <div class="row d-flex vh-100 align-items-center justify-content-center">
        <div class="col-4">
            <form class="row border bg-light" name="registerForm" ng-controller="RegisterController" novalidate>

                <div class="col-12 d-flex justify-content-center border-bottom">
                    <h3 class="form-label"><fmt:message key="register.label.register"/></h3>
                </div>

                <div ng-if="errorMessage" class="col-12 d-flex justify-content-center">
                    <span class="form-text text-danger">{{ errorMessage }}</span>
                </div>

                <div class="col-3 mt-3">
                    <h5>
                        <label for="firstnameField" class="form-label">
                            <fmt:message key="register.label.firstname"/>
                        </label>
                    </h5>
                </div>

                <div class="col-9 mt-3">
                    <input type="text" class="form-control" name="firstname" id="firstnameField"
                           ng-model="firstname"
                           ng-class="{'is-invalid': registerForm.firstname.$touched && registerForm.firstname.$invalid}"
                           required>
                </div>

                <div class="col-3 mt-3">
                    <h5>
                        <label for="lastnameField" class="form-label">
                            <fmt:message key="register.label.lastname"/>
                        </label>
                    </h5>
                </div>

                <div class="col-9 mt-3">
                    <input type="text" class="form-control" name="lastname" id="lastnameField"
                           ng-model="lastname"
                           ng-class="{'is-invalid': registerForm.lastname.$touched && registerForm.lastname.$invalid}"
                           required>
                </div>

                <div class="col-3 mt-3">
                    <h5>
                        <label for="loginField" class="form-label"><fmt:message key="register.label.login"/></label>
                    </h5>
                </div>

                <div class="col-9 mt-3">
                    <input type="text" class="form-control" name="login" id="loginField"
                           ng-class="{'is-invalid': registerForm.login.$touched && registerForm.login.$invalid}"
                           ng-model="login"
                           required>
                </div>

                <div class="col-3 mt-3">
                    <h5>
                        <label for="emailField" class="form-label">
                            <fmt:message key="register.label.email"/>
                        </label>
                    </h5>
                </div>

                <div class="col-9 mt-3">
                    <input type="email" class="form-control" name="email" id="emailField"
                           ng-model="email"
                           ng-class="{'is-invalid': registerForm.email.$touched && registerForm.email.$invalid}"
                           required>
                </div>

                <div class="col-3 mt-3">
                    <h5>
                        <label for="passwordField" class="form-label">
                            <fmt:message key="register.label.password"/>
                        </label>
                    </h5>
                </div>

                <div class="col-9 mt-3">
                    <input type="password" class="form-control" name="password" id="passwordField"
                           ng-class="{'is-invalid': registerForm.password.$touched && registerForm.password.$invalid}"
                           ng-model="password" required>
                </div>

                <div class="col-3 mt-3">
                    <h5>
                        <label for="password2Field" class="form-label">
                            <fmt:message key="register.label.password2"/>
                        </label>
                    </h5>
                </div>

                <div class="col-9 mt-3">
                    <input type="password" class="form-control" name="password2" id="password2Field"
                           ng-class="{'is-invalid': registerForm.password2.$touched && registerForm.password2.$invalid}"
                           ng-model="password2" required>
                </div>


                <div class="col-12 d-flex justify-content-end border-top mt-2 pt-4 pb-4">
                    <button type="submit" class="btn btn-primary " ng-click="userRegister()"
                            ng-disabled="registerForm.$invalid || (password !== password2)">
                        <fmt:message key="register.button.register"/>
                    </button>

                    <button type="submit" class="btn btn-secondary ms-3" ng-click="back()">
                        <fmt:message key="register.button.back"/>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    const baseUrl = '${pageContext.request.contextPath}';
</script>

<script src="${pageContext.request.contextPath}/static/js/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/moment.js"></script>

<script src="${pageContext.request.contextPath}/static/js/expositions.module.js"></script>
<script src="${pageContext.request.contextPath}/static/js/expositions.service.js"></script>
<script src="${pageContext.request.contextPath}/static/js/register.controller.js"></script>
</body>
</html>
