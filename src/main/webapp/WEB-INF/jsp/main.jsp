<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--@elvariable id="currentLanguage" type="java.lang.String"--%>

<fmt:setLocale value="${currentLanguage}"/>
<fmt:setBundle basename="messages"/>

<!doctype html >
<html lang="en" ng-app="expositions">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Expositions site</title>

    <jsp:include page="header.jsp"/>

</head>
<body>

<div class="container-fluid" ng-controller="ExpositionsController">

    <%-----------------------nav bar------------------------------%>

    <form class="row mt-2 p-2 bg-light">
        <div class="col-auto">
            <select class="form-select" aria-label="Select Language" name="lang" id="langSelect"
                    ng-model="currentLanguage"
                    ng-options="lang.toUpperCase() for lang in availableLangs"
                    ng-change="langChanged()">
            </select>
        </div>

        <div class="ms-auto col-auto py-2">
            <span>{{user.login}}</span>
        </div>

        <div class="col-auto">
            <button ng-if="user.isAuthorized" ng-click="logout()" class="btn btn-outline-dark" tabindex="-1"
                    role="button">
                <fmt:message key="label.logOut"/>
            </button>

            <button ng-if="!user.isAuthorized" ng-click="login()" class="btn btn-outline-dark" tabindex="-1"
                    role="button">
                <fmt:message key="label.LogIn"/>
            </button>

            <button ng-if="!user.isAuthorized" ng-click="register()" class="btn btn-outline-dark" tabindex="-1"
                    role="button">
                <fmt:message key="label.register"/>
            </button>

        </div>
    </form>

    <%-----------------------nav bar end-------------------%>

    <%--------------------filter---------------------------%>
    <form class="row mt-5 justify-content-center align-items-center">

        <div class="col-auto">
            <label for="filterDateFrom">
                <span><fmt:message key="label.dateFrom"/>:</span>
            </label>
        </div>
        <div class="col-auto">
            <input type="date" name="dateFrom" id="filterDateFrom"
                   ng-model="filter.dateFrom">
        </div>

        <div class="col-auto">
            <label for="filterDateTo">
                <span><fmt:message key="label.dateTo"/>:</span>
            </label>
        </div>
        <div class="col-auto">
            <input type="date" name="dateTo" id="filterDateTo"
                   ng-model="filter.dateTo">
        </div>

        <div class="col-auto">
            <button type="button" ng-click="filterExposition()" class="btn btn-outline-dark" id="filterButton">
                <fmt:message key="button.filter"/>
            </button>
        </div>

        <div ng-if="user.isAdmin" class="col-auto">
            <button type="button" ng-click="addExposition()" class="btn btn-outline-dark" tabindex="-1" role="button">
                <fmt:message key="button.addExposition"/>
            </button>
        </div>

    </form>
    <%--------------------filter end ---------------------------%>

    <%-------------------- Table --------------------%>
    <div class="row mt-5">
        <div class="offset-1 col-9">
            <table class="table">
                <thead class="table-light">
                <tr>
                    <th scope="col" ng-click="sortTableColumn('topic')">
                        <span role="button" id="columnTopic" data-column="topic">
                            <fmt:message key="table.topic"/>
                        </span>
                        <i ng-if="sort.column === 'topic'"
                           ng-class="{'bi-caret-down-fill': !sort.ascending, 'bi-caret-up-fill': sort.ascending}"
                           class='bi'></i>
                    </th>
                    <th scope="col" ng-click="sortTableColumn('dateFrom')">
                        <span role="button" id="columnDateFrom" data-column="dateFrom">
                            <fmt:message key="table.dateFrom"/>
                        </span>
                        <i ng-if="sort.column === 'dateFrom'"
                           ng-class="{'bi-caret-down-fill': !sort.ascending, 'bi-caret-up-fill': sort.ascending}"
                           class='bi'></i>
                    </th>
                    <th scope="col" ng-click="sortTableColumn('dateTo')">
                        <span role="button" id="columnDateTo" data-column="dateTo">
                            <fmt:message key="table.dateTo"/>
                        </span>
                        <i ng-if="sort.column === 'dateTo'"
                           ng-class="{'bi-caret-down-fill': !sort.ascending, 'bi-caret-up-fill': sort.ascending}"
                           class='bi'></i>
                    </th>

                    <th scope="col" ng-click="sortTableColumn('price')">
                        <span role="button" id="columnPrice" data-column="price">
                            <fmt:message key="table.price"/>
                        </span>
                        <i ng-if="sort.column === 'price'"
                           ng-class="{'bi-caret-down-fill': !sort.ascending, 'bi-caret-up-fill': sort.ascending}"
                           class='bi'></i>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="exp in expositions">
                    <td><a ng-href="{{ getExpositionLink(exp.id) }}">{{exp.topic | limitTo:50}}</a></td>
                    <td>{{ exp.dateFrom | date:'dd-MM-yyyy'}}</td>
                    <td>{{exp.dateTo | date:'dd-MM-yyyy'}}</td>
                    <td>{{exp.price | currency:'':2}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <%-------------------- Table end --------------------%>

    <%-------------------- Pagination start --------------------%>
    <div class="row">
        <div class="mx-auto col-2">
            <paginator current-page="pager.page"
                       total-pages="pager.total"
                       on-page-change="changePage(page)"></paginator>
        </div>
    </div>

    <%-------------------- Pagination end --------------------%>
</div>

<script>
    const baseUrl = '${pageContext.request.contextPath}';
</script>

<script src="${pageContext.request.contextPath}/static/js/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/moment.js"></script>

<script src="${pageContext.request.contextPath}/static/js/expositions.module.js"></script>
<script src="${pageContext.request.contextPath}/static/js/expositions.service.js"></script>
<script src="${pageContext.request.contextPath}/static/js/paginator.component.js"></script>
<script src="${pageContext.request.contextPath}/static/js/expositions.controller.js"></script>
</body>
</html>
