<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="paginator" type="org.example.expositions.util.Paginator" required="true" %>

<fmt:setLocale value="${requestScope.get('currentLanguage')}"/>
<fmt:setBundle basename="messages"/>

<%
    if (paginator.getTotalPages() == 1) {
        return;
    }
%>

<ul class="pagination justify-content-center">

    <li class="page-item ${paginator.firstPage ? 'disabled' : ''}" data-bs-toggle="tooltip"
        title="<fmt:message key="paginator.firstPage.tooltip"/>"
        data-page="1">
        <a class="page-link" role="button">
            <i class="bi bi-chevron-bar-left"></i>
        </a>
    </li>

    <li class="page-item ${paginator.firstPage ? 'disabled': ''}" data-bs-toggle="tooltip"
        title="<fmt:message key="paginator.previousPage.tooltip"/>"
        data-page="${paginator.previousPage}">
        <a class="page-link">
            <i class="bi bi-chevron-left"></i>
        </a>
    </li>

    <c:if test="${paginator.previousPage != paginator.currentPage}">
        <li class="page-item" data-page="${paginator.previousPage}">
            <a class="page-link" role="button">${paginator.previousPage}</a>
        </li>
    </c:if>

    <li class="page-item active" data-page="${paginator.currentPage}">
        <a class="page-link" role="button">${paginator.currentPage}</a>
    </li>

    <c:if test="${paginator.nextPage != paginator.currentPage}">
        <li class="page-item" data-page="${paginator.nextPage}">
            <a class="page-link" role="button">${paginator.nextPage}</a>
        </li>
    </c:if>

    <li class="page-item ${paginator.lastPage ? 'disabled': ''}" data-bs-toggle="tooltip"
        title="<fmt:message key="paginator.nextPage.tooltip"/>"
        data-page="${paginator.nextPage}">
        <a class="page-link" role="button">
            <i class="bi bi-chevron-right"></i>
        </a>
    </li>

    <li class="page-item ${paginator.lastPage ? 'disabled': ''}" data-bs-toggle="tooltip"
        title="<fmt:message key="paginator.lastPage.tooltip"/>"
        data-page="${paginator.totalPages}">
        <a class="page-link" role="button">
            <i class="bi bi-chevron-bar-right"></i>
        </a>
    </li>
</ul>
