<%@ tag import="java.time.format.DateTimeFormatter" %>

<%@ attribute name="value" type="java.time.LocalDate" required="true" %>

<%= value.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")) %>
