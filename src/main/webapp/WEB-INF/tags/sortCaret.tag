<%@ tag import="java.util.Objects" %>
<%@ attribute name="current" type="java.lang.String" required="true" %>
<%@ attribute name="column" type="java.lang.String" required="true" %>
<%@ attribute name="order" type="java.lang.String" required="false" %>

<%
    if (Objects.equals(current, column)) {
        if ("DSC".equals(order)) {
            out.print("<i class='bi bi-caret-down-fill'></i>");
        } else {
            out.print("<i class='bi bi-caret-up-fill'></i>");
        }
    }
%>
