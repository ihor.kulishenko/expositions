<%@ tag import="java.lang.String, java.lang.Integer" %>

<%@ attribute name="value" type="java.lang.String" required="true" %>
<%@ attribute name="max" type="java.lang.String" required="false" %>

<%
    final int maxAttribute = jspContext.getAttribute("max") != null
            ? Integer.parseInt((String) jspContext.getAttribute("max")) : 20;
%>

<%= value.substring(0, Math.min(value.length(), maxAttribute))
        .concat(value.length() > maxAttribute ? "..." : "") %>
