<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="lang" type="java.lang.String" required="true" %>

<select class="form-select" aria-label="Select Language" name="lang" id="langSelect">
    <option value="en"
            <c:if test="${lang == 'en'}">selected</c:if> >EN
    </option>
    <option value="uk"
            <c:if test="${lang == 'uk'}">selected</c:if>>UK
    </option>
</select>
