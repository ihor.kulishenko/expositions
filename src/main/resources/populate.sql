begin;

insert into public.exposition (topic, start, finish, ticket_price, status)
values ('Exposition topic 1', '2021-12-10'::date, '2021-12-31'::date, 100.00, 'ACTIVE'::public.exposition_status);

insert into public.exposition (topic, start, finish, ticket_price, status)
values ('Winter Exposition topic 1', '2021-12-28'::date, '2022-01-12'::date, 10.00, 'ACTIVE'::public.exposition_status);
commit;

begin;
insert into public.exposition (topic, start, finish, ticket_price, status)
values ('Экспозиция с национальными символами', '2021-12-28'::date, '2022-01-12'::date, 10.00,
        'ACTIVE'::public.exposition_status);
commit;

begin;
insert into public.exposition (topic, start, finish, ticket_price, status)
values ('Сучасна українска виставка робіт майстрів', '2022-01-01'::date, '2022-02-01'::date, 0.00,
        'ACTIVE'::public.exposition_status);
commit;

begin;
insert into public."user" (first_name, last_name, login, password, email, role)
VALUES ('userFirstname', 'userLastname', 'user', '$2a$10$MqzggAAlwhrsu.Bo9Ni2G.qF4e3gCdd49.yChmpXBXMebR8FbUQDi', -- user
        'user@mail.org', 'USER'::user_role);
commit;

begin;
insert into public."user" (first_name, last_name, login, password, email, role)
VALUES ('adminFirstname', 'adminLastname', 'admin',
        '$2b$10$Hjn1XxwpvMf0CLjOI7CGyeoMz7XN/wfYY2CcA.3FrsltKJ74hXlYG', -- admin
        'admin@mail.org', 'ADMINISTRATOR'::public.user_role);
commit;

begin;
insert into public.hall (name)
values ('Exposition Hall 1'),
       ('Exposition Hall 2'),
       ('Exposition Hall 3');
commit;
