/*
create database exposition with
    LC_COLLATE 'uk_UA.utf8'
    LC_CTYPE 'uk_UA.utf8'
    ENCODING 'UTF8'
    TEMPLATE template0
    owner exposition_admin;
*/
begin;

drop table if exists exposition_hall;
drop table if exists ticket;
drop table if exists schedule;
drop table if exists exposition;
drop table if exists hall;
drop table if exists "user";

drop type if exists exposition_status;
drop type if exists user_role;
drop type if exists weekday_type;

drop index if exists exposition_hall_exposition_id_index;


create type exposition_status as enum ('ACTIVE', 'CANCELED');

create table exposition
(
    id           serial primary key,
    topic        text              not null,
    start        date              not null,
    finish       date              not null,
    ticket_price numeric(8, 2)     not null,
    status       exposition_status not null
);

create table hall
(
    id   serial primary key,
    name text not null
);

create table exposition_hall
(
    exposition_id bigint not null references exposition (id),
    hall_id       bigint not null references hall (id),
    primary key (exposition_id, hall_id)
);

create index exposition_hall_exposition_id_index on exposition_hall(exposition_id);

create type user_role as enum ('ANONYMOUS', 'USER', 'ADMINISTRATOR');

create table "user"
(
    id         serial primary key,
    first_name text      not null,
    last_name  text      not null,
    login      text      not null unique,
    password   text      not null,
    email      text      not null,
    role       user_role not null
);

create table ticket
(
    id            serial primary key,
    user_id       bigint        not null references "user" (id),
    exposition_id bigint        not null references exposition (id),
    date          date          not null,
    price         numeric(8, 2) not null
);

create type weekday_type as enum ('MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY');

create table schedule
(
    id            serial primary key,
    exposition_id bigint       not null references exposition (id),
    weekday       weekday_type not null,
    start         time         not null,
    finish        time         not null,
    unique (exposition_id, weekday)
);

alter type exposition_status owner to exposition_admin;
alter type user_role owner to exposition_admin;
alter type weekday_type owner to exposition_admin;
alter table exposition owner to exposition_admin;
alter table hall owner to exposition_admin;
alter table exposition_hall owner to exposition_admin;
alter table "user" owner to exposition_admin;
alter table ticket owner to exposition_admin;
alter table schedule owner to exposition_admin;

commit;
