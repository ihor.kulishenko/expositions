package org.example.expositions.factory;

import org.example.expositions.dto.FilteredExpositionsDTO;
import org.example.expositions.util.FilteredExposition;

import static org.example.expositions.util.ExpositionFilter.Order.ASCENDING;

public class FilteredExpositionsFactory {

    public static FilteredExpositionsDTO createDTO(FilteredExposition vo) {
        return new FilteredExpositionsDTO(vo.getDateFrom(), vo.getDateTo(),
                ExpositionFactory.createDTOList(vo.getExpositions()),
                vo.getSortColumn().getDtoColumnName(), vo.getSortOrder() == ASCENDING ? "asc" : "desc",
                vo.getCurrentPage(), vo.getTotalPages(), vo.getElementsPerPage());
    }
}
