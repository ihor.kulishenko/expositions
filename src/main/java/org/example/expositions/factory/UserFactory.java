package org.example.expositions.factory;

import org.example.expositions.dto.UserDTO;
import org.example.expositions.entity.Role;
import org.example.expositions.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Uses to produce user entity from dto and vise versa
 */
public class UserFactory {
    public static User createEntity(ResultSet resultSet) throws SQLException {
        return new User(
                resultSet.getLong("usr_id"),
                resultSet.getString("usr_first_name"),
                resultSet.getString("usr_last_name"),
                resultSet.getString("usr_login"),
                resultSet.getString("usr_password"),
                resultSet.getString("usr_email"),
                Role.valueOf(resultSet.getString("usr_role")));
    }

    public static UserDTO createDTO(User user) {
        return new UserDTO(
                user.getFirstName(),
                user.getLastName(),
                user.getLogin(),
                user.getEmail(),
                user.getRole() == Role.ADMINISTRATOR
        );
    }
}
