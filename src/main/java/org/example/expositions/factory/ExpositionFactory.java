package org.example.expositions.factory;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.example.expositions.dto.ExpositionDTO;
import org.example.expositions.dto.ScheduleDTO;
import org.example.expositions.entity.Exposition;
import org.example.expositions.entity.ExpositionStatus;
import org.example.expositions.entity.Hall;
import org.example.expositions.entity.Schedule;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Uses to produce exposition entity from dto and vise versa
 */
public class ExpositionFactory {
    public static Exposition createEntity(ResultSet rs) throws SQLException {
        return new Exposition(
                rs.getLong("exp_id"),
                rs.getString("exp_topic"),
                rs.getObject("exp_start", LocalDate.class),
                rs.getObject("exp_finish", LocalDate.class),
                rs.getBigDecimal("exp_ticket_price"),
                ExpositionStatus.valueOf(rs.getString("exp_status"))
        );
    }

    public static Exposition createEntity(ExpositionDTO dto) {
        return new Exposition(0L, dto.getTopic(), dto.getDateFrom(), dto.getDateTo(), dto.getTicketPrice(),
                ExpositionStatus.ACTIVE);
    }

    public static ExpositionDTO createDTO(String jsonString) throws IOException {
        final var module = new SimpleModule();
        module.addDeserializer(ScheduleDTO.class, new ScheduleDTODeserializer());

        final var objectMapper = JsonMapper.builder()
                .addModule(new JavaTimeModule())
                .addModule(module)
                .build();

        return objectMapper.readValue(jsonString, ExpositionDTO.class);
    }

    public static ExpositionDTO createDTO(String language, Exposition exposition, List<Hall> halls, List<Schedule> schedule) {
        final var dto = new ExpositionDTO();

        dto.setId(exposition.getId());
        dto.setTopic(exposition.getTopic());
        dto.setDateFrom(exposition.getStart());
        dto.setDateTo(exposition.getFinish());
        dto.setTicketPrice(exposition.getTicketPrice());
        dto.setHalls(HallFactory.createDTOList(halls));
        dto.setSchedule(ScheduleFactory.createDTOList(language, schedule));

        return dto;
    }

    public static ExpositionDTO createDTO(Exposition exposition) {
            final var dto = new ExpositionDTO();

            dto.setId(exposition.getId());
            dto.setTopic(exposition.getTopic());
            dto.setDateFrom(exposition.getStart());
            dto.setDateTo(exposition.getFinish());
            dto.setTicketPrice(exposition.getTicketPrice());
            dto.setHalls(Collections.emptyList());
            dto.setSchedule(Collections.emptyList());

            return dto;
        }

    public static List<ExpositionDTO> createDTOList(List<Exposition> list) {
        if (list == null || list.isEmpty()) {
            return Collections.emptyList();
        }

        return list.stream()
                .map(ExpositionFactory::createDTO)
                .collect(Collectors.toList());
    }
}
