package org.example.expositions.factory;

import org.example.expositions.dto.ExpositionDTO;
import org.example.expositions.dto.HallDTO;
import org.example.expositions.entity.Hall;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Uses to produce hall entity from dto and vise versa
 */
public class HallFactory {
    public static Hall createEntity(ResultSet rs) throws SQLException {
        return new Hall(rs.getLong("hl_id"), rs.getString("hl_name"));
    }

    public static List<Hall> createEntityList(ResultSet rs) throws SQLException {
        final var result = new ArrayList<Hall>();

        while (rs.next()) {
            result.add(createEntity(rs));
        }
        return result;
    }

    public static Hall createEntity(HallDTO dto) {
        return new Hall(dto.getId(), dto.getName());
    }

    public static List<Hall> createEntityList(ExpositionDTO dto) {
        return dto.getHalls().stream()
                .map(HallFactory::createEntity)
                .collect(Collectors.toList());
    }

    private static HallDTO createDTO(Hall entity) {
        return new HallDTO(entity.getId(), entity.getName());
    }

    public static List<HallDTO> createDTOList(List<Hall> entityList) {
        return entityList.stream()
                .map(HallFactory::createDTO)
                .collect(Collectors.toList());
    }
}
