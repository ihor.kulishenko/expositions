package org.example.expositions.factory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.example.expositions.dto.ScheduleDTO;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Uses to deserialize schedule dto to JSON
 */
public class ScheduleDTODeserializer extends StdDeserializer<ScheduleDTO> {
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

    public ScheduleDTODeserializer() {
        this(null);
    }

    public ScheduleDTODeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public ScheduleDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        final var jsonNode = (JsonNode) jsonParser.getCodec().readTree(jsonParser);

        final var weekday = jsonNode.get("weekday").asText();
        final var timeFrom = LocalTime.parse(jsonNode.get("timeFrom").asText(), TIME_FORMATTER);
        final var timeTo = LocalTime.parse(jsonNode.get("timeTo").asText(), TIME_FORMATTER);

        return new ScheduleDTO(weekday, timeFrom, timeTo);
    }
}
