package org.example.expositions.factory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.example.expositions.dto.TicketDTO;
import org.example.expositions.entity.Ticket;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Uses to produce ticket entity from dto and vise versa
 */
public class TicketFactory {
    public static Ticket createEntity(ResultSet rs) throws SQLException {
        return new Ticket(
                rs.getLong("tck_id"),
                UserFactory.createEntity(rs),
                ExpositionFactory.createEntity(rs),
                rs.getObject("tck_date", LocalDate.class),
                rs.getBigDecimal("tck_price")
        );
    }

    public static TicketDTO createDTO(String jsonString) throws JsonProcessingException {
        final var objectMapper = JsonMapper.builder()
                .addModule(new JavaTimeModule())
                .build();

        return objectMapper.readValue(jsonString, TicketDTO.class);
    }
}
