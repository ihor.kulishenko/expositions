package org.example.expositions.factory;

import org.example.expositions.dto.ExpositionFilterDTO;
import org.example.expositions.util.ExpositionFilter;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ExpositionFilterFactory {
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static ExpositionFilterDTO createDTO(HttpServletRequest request) {
        return new ExpositionFilterDTO(
                toLocalDate(request.getParameter("dateFrom")),
                toLocalDate(request.getParameter("dateTo")),
                toColumn(request.getParameter("column")),
                toSortOrder(request.getParameter("order")),
                toInteger(request.getParameter("page")),
                toInteger(request.getParameter("elementsPerPage"))
        );
    }

    public static ExpositionFilter createVO(ExpositionFilterDTO dto) {

        final var builder = ExpositionFilter.builder();
        if (dto.getDateFrom() != null) {
            builder.addDateFrom(dto.getDateFrom());
        }

        if (dto.getDateTo() != null) {
            builder.addDateTo(dto.getDateTo());
        }

        if (dto.getSortColumn() != null) {
            builder.sortBy(dto.getSortColumn());
        }

        if (dto.getSortOrder() != null) {
            builder.sortOrder(dto.getSortOrder());
        }

        if (dto.getPage() != null) {
            builder.addPage(dto.getPage());
        }

        if (dto.getElementsPerPage() != null) {
            builder.addElementsPerPage(dto.getElementsPerPage());
        }

        return builder.build();
    }

    private static LocalDate toLocalDate(String str) {
        try {
            return LocalDate.parse(str, DATE_FORMAT);
        } catch (Exception e) {
            return null;
        }
    }

    private static ExpositionFilter.Column toColumn(String str) {
        for (ExpositionFilter.Column column : ExpositionFilter.Column.values()) {
            if (column.getDtoColumnName().equalsIgnoreCase(str)) {
                return column;
            }
        }

        return null;
    }

    private static ExpositionFilter.Order toSortOrder(String str) {
        return "desc".equalsIgnoreCase(str) ? ExpositionFilter.Order.DESCENDING : ExpositionFilter.Order.ASCENDING;
    }

    private static Integer toInteger(String str) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            return null;
        }
    }
}
