package org.example.expositions.factory;

import org.example.expositions.dto.ExpositionDTO;
import org.example.expositions.dto.ScheduleDTO;
import org.example.expositions.entity.Schedule;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * Uses to produce schedule entity from dto and vise versa
 */
public class ScheduleFactory {
    public static Schedule createEntity(ResultSet rs) throws SQLException {

        return new Schedule(
                rs.getLong("sc_id"),
                rs.getLong("sc_exposition_id"),
                DayOfWeek.valueOf(rs.getString("sc_weekday")),
                rs.getObject("sc_start", LocalTime.class),
                rs.getObject("sc_finish", LocalTime.class)
        );
    }

    public static List<Schedule> createEntityList(ResultSet rs) throws SQLException {
        final var result = new ArrayList<Schedule>();

        while (rs.next()) {
            final var schedule = new Schedule(
                    rs.getLong("sc_id"),
                    rs.getLong("sc_exposition_id"),
                    DayOfWeek.valueOf(rs.getString("sc_weekday")),
                    rs.getObject("sc_start", LocalTime.class),
                    rs.getObject("sc_finish", LocalTime.class));

            result.add(schedule);
        }
        return result;
    }

    public static Schedule createEntity(ScheduleDTO dto) {
        return new Schedule(0L, 0L, DayOfWeek.valueOf(dto.getWeekdayCode()), dto.getTimeFrom(), dto.getTimeTo());
    }

    public static List<Schedule> createEntityList(ExpositionDTO dto) {
        return dto.getSchedule().stream().map(ScheduleFactory::createEntity).collect(Collectors.toList());
    }

    public static ScheduleDTO createDTO(String language, Schedule entity) {
        final var resourceBundle = ResourceBundle.getBundle("messages", new Locale(language));
        final var localizedWeekday = resourceBundle.getString(entity.getDayOfWeek().name());

        return new ScheduleDTO(entity.getDayOfWeek().name(), localizedWeekday, entity.getStart(), entity.getFinish());
    }

    public static List<ScheduleDTO> createDTOList(String language, List<Schedule> entityList) {
        return entityList.stream()
                .map(_s -> createDTO(language, _s))
                .collect(Collectors.toList());
    }
}
