package org.example.expositions.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Utility class for validation purposes
 *
 * @param <T> - validating class
 */
public class Validator<T> {
    public final static Validator.Result OK = new Validator.Result(true, "");

    private final List<Holder<T>> predicateList = new LinkedList<>();

    public static <T, U> Predicate<T> nonNullField(Function<T, U> keyExtractor) {
        return _e -> keyExtractor.apply(_e) != null;
    }

    public static <T, U extends String> Predicate<T> nonEmptyField(Function<T, U> keyExtractor) {
        return _e -> _e != null && keyExtractor.apply(_e) != null && !keyExtractor.apply(_e).isBlank();
    }

    public static <T, U extends LocalDate> Predicate<T> dateRangeFields(
            Function<T, U> dateFromExtractor, Function<T, U> dateToExtractor) {

        return _e -> {
            final var dateFrom = dateFromExtractor.apply(_e);
            final var dateTo = dateToExtractor.apply(_e);

            return dateFrom != null && dateTo != null && dateFrom.isBefore(dateTo);
        };
    }

    public static <T, U extends BigDecimal> Predicate<T> nonNegativeField(Function<T, U> keyExtractor) {
        return _e -> {
            final var value = keyExtractor.apply(_e);

            return value != null && value.signum() >= 0;
        };
    }

    public static <T, U extends List<?>> Predicate<T> emptyListField(Function<T, U> keyExtractor) {
        return _e -> {
            final var value = keyExtractor.apply(_e);
            return value != null && !value.isEmpty();
        };
    }

    public static <T extends List<V>, U, V> Predicate<T> uniqueValueInList(Function<V, U> keyExtractor) {
        return _e -> {
            final var set = new HashSet<U>();
            for (V element : _e) {
                final var value = keyExtractor.apply(element);
                if (set.contains(value)) {
                    return false;
                }
                set.add(value);
            }

            return true;
        };
    }

    public Validator<T> add(Predicate<T> predicate, String errorMessage) {
        if (predicate != null) {
            predicateList.add(new Holder<>(predicate, errorMessage));
        }

        return this;
    }

    public Validator<T> add(Validator<T> otherValidator) {
        if (otherValidator != null) {
            predicateList.addAll(otherValidator.predicateList);
        }

        return this;
    }

    public Result validate(T object) {

        for (Holder<T> element : predicateList) {
            final var result = element.predicate.test(object);
            if (!result) {
                return new Result(false, element.errorMessage);
            }
        }

        return new Result(true, "");
    }

    public static class Result {
        public final boolean success;
        public final String errorMessage;

        public Result(boolean success, String errorMessage) {
            this.success = success;
            this.errorMessage = errorMessage;
        }
    }

    private static class Holder<T> {
        private final Predicate<T> predicate;
        private final String errorMessage;

        private Holder(Predicate<T> predicate, String errorMessage) {
            this.predicate = Objects.requireNonNull(predicate);
            this.errorMessage = Objects.requireNonNull(errorMessage);
        }
    }
}
