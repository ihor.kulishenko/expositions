package org.example.expositions.util;

import at.favre.lib.crypto.bcrypt.BCrypt;

public interface PasswordUtils {
    int DEFAULT_COST = 10;

    static String encode(String plainPassword) {
        return BCrypt.withDefaults().hashToString(DEFAULT_COST, plainPassword.toCharArray());
    }

    static boolean check(String plainPassword, String hash) {
        return BCrypt.verifyer().verify(plainPassword.toCharArray(), hash).verified;
    }
}
