package org.example.expositions.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.example.expositions.dao.AppException;
import org.example.expositions.delegate.Result;
import org.example.expositions.dto.ResultDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import java.util.stream.Collectors;

public interface ServletUtils {
    static void sendResult(ResultDTO resultDTO, HttpServletResponse response) {
        try {
            final var objectMapper = JsonMapper.builder()
                    .addModule(new JavaTimeModule())
                    .build();

            final var jsonString = objectMapper.writeValueAsString(resultDTO);

            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType("application/json");
            response.getWriter().write(jsonString);
        } catch (Exception e) {
            throw new AppException(e);
        }
    }

    static void sendError(HttpServletResponse response, String message) {
        sendResult(toResultDTO(Result.failure(message)), response);
    }

    static void sendOk(HttpServletResponse response, Result<?> result) {
        sendResult(toResultDTO(result), response);
    }

    static ResultDTO toResultDTO(Result<?> result) {
        if (result == null) {
            return ResultDTO.failure("result is not set");
        } else if (result.failure()) {
            return ResultDTO.failure(result.message);
        } else {
            return ResultDTO.ok(result.container);
        }
    }

    static String getBodyFromRequest(HttpServletRequest request) {
        try {
            return request.getReader().lines().collect(Collectors.joining());
        } catch (Exception e) {
            throw new AppException(e);
        }
    }

    @SuppressWarnings("unchecked")
    static <T> Optional<T> getParameter(HttpServletRequest request, String parameterName, Class<T> clazz) {
        try {
            final var parameter = request.getParameter(parameterName);
            if (parameter == null) {
                return Optional.empty();
            }

            if (clazz == Long.class) {
                return (Optional<T>) Optional.of(Long.parseLong(parameter));
            } else if (clazz == Integer.class) {
                return (Optional<T>) Optional.of(Integer.parseInt(parameter));
            } else if (clazz == LocalDate.class) {
                return (Optional<T>) Optional.of(LocalDate.parse(parameter, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            } else if (clazz == String.class) {
                return (Optional<T>) Optional.of(parameter);
            } else {
                throw new AppException("unknown type " + clazz);
            }
        } catch (NumberFormatException | DateTimeParseException e) {
            return Optional.empty();
        }
    }
    static <T> T getDto(HttpServletRequest request, Class<T> dtoClass) {
        try {
            final var body = ServletUtils.getBodyFromRequest(request);
            final var objectMapper = new ObjectMapper();
            return objectMapper.readValue(body, dtoClass);
        } catch (Exception e) {
            throw new AppException(e);
        }
    }
}
