package org.example.expositions.util;

import org.example.expositions.entity.Role;
import org.example.expositions.entity.User;

import javax.servlet.http.HttpServletRequest;

public interface SessionUtils {
    String DEFAULT_LANGUAGE = "en";

    static User getCurrentUser(HttpServletRequest request) {
        if (request == null) {
            return null;
        }

        final var session = request.getSession(false);
        if (session == null) {
            return null;
        }

        return (User) session.getAttribute(ATTRIBUTES.user.name());
    }

    static boolean isAuthorized(HttpServletRequest request) {
        return getCurrentUser(request) != null;
    }

    static boolean isAdmin(HttpServletRequest request) {
        final var user = getCurrentUser(request);

        return user != null && user.getRole() == Role.ADMINISTRATOR;
    }

    static void setLanguage(HttpServletRequest request, String language) {
        final var session = request.getSession(false);
        if (session != null) {
            session.setAttribute(ATTRIBUTES.currentLanguage.name(), language);
        }
    }

    static String getLanguage(HttpServletRequest request) {
        final var session = request.getSession(false);
        if (session != null) {
            final var language = ((String) session.getAttribute(ATTRIBUTES.currentLanguage.name()));

            return language != null ? language : DEFAULT_LANGUAGE;
        }

        return DEFAULT_LANGUAGE;
    }

    enum ATTRIBUTES {user, currentLanguage}

}
