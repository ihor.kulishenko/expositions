package org.example.expositions.util;

import org.example.expositions.entity.Exposition;

import java.time.LocalDate;
import java.util.List;
import java.util.StringJoiner;

public class FilteredExposition {
    private final LocalDate dateFrom;
    private final LocalDate dateTo;
    private final List<Exposition> expositions;
    private final ExpositionFilter.Column sortColumn;
    private final ExpositionFilter.Order sortOrder;
    private final long currentPage;
    private final long totalPages;
    private final long elementsPerPage;

    public FilteredExposition(LocalDate dateFrom, LocalDate dateTo, List<Exposition> expositions,
                              ExpositionFilter.Column sortColumn, ExpositionFilter.Order sortOrder,
                              long currentPage, long totalPages, long elementsPerPage) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.expositions = expositions;
        this.sortColumn = sortColumn;
        this.sortOrder = sortOrder;
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.elementsPerPage = elementsPerPage;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public List<Exposition> getExpositions() {
        return expositions;
    }

    public ExpositionFilter.Column getSortColumn() {
        return sortColumn;
    }

    public ExpositionFilter.Order getSortOrder() {
        return sortOrder;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public long getElementsPerPage() {
        return elementsPerPage;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", FilteredExposition.class.getSimpleName() + "[", "]")
                .add("expositions=" + expositions)
                .add("sortColumn=" + sortColumn)
                .add("sortOrder=" + sortOrder)
                .add("currentPage=" + currentPage)
                .add("totalPages=" + totalPages)
                .add("elementsPerPage=" + elementsPerPage)
                .toString();
    }
}
