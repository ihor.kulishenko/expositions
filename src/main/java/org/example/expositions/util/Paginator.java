package org.example.expositions.util;

import java.util.List;

/**
 * Helps paginating pages
 */
public class Paginator {
    private final int totalPages;
    private final int totalElements;
    private final int elementsPerPage;
    private int currentPage;

    public Paginator(int totalElements, int elementsPerPage) {
        this.currentPage = 1;
        this.totalElements = Math.max(totalElements, 0);
        this.elementsPerPage = Math.max(elementsPerPage, 0);

        if (this.totalElements == 0 || this.elementsPerPage == 0) {
            totalPages = 1;
        } else {
            totalPages = this.totalElements / this.elementsPerPage + (this.totalElements % this.elementsPerPage > 0 ? 1 : 0);
        }

    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int page) {
        this.currentPage = page <= 0 ? 1 : Math.min(page, totalPages);
    }

    public int getPreviousPage() {
        return Math.max(1, currentPage - 1);
    }

    public int getNextPage() {
        return Math.min(totalPages, currentPage + 1);
    }

    public int getTotalPages() {
        return totalPages;
    }

    public boolean isFirstPage() {
        return currentPage == 1;
    }

    public boolean isLastPage() {
        return currentPage == totalPages;
    }

    public <T> List<T> getElements(List<T> list) {
        if (list == null || list.isEmpty()) {
            return list;
        }

        final var from = (currentPage - 1) * elementsPerPage;
        final var to = Math.min(from + elementsPerPage, totalElements);

        return list.subList(from, to);
    }
}
