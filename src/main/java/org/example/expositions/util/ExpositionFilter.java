package org.example.expositions.util;

import org.example.expositions.entity.ExpositionStatus;

import java.time.LocalDate;
import java.util.EnumSet;
import java.util.StringJoiner;

/**
 * Holds filter parameters for filtering expositions
 */
public class ExpositionFilter {
    public static final int ELEMENTS_PER_PAGE = 10;

    private final LocalDate dateFrom;
    private final LocalDate dateTo;
    private final EnumSet<ExpositionStatus> statuses;
    private final Column sortColumn;
    private final Order sortOrder;
    private final int page;
    private final int elementsPerPage;

    public ExpositionFilter(LocalDate dateFrom, LocalDate dateTo, EnumSet<ExpositionStatus> statuses,
                            Column sortColumn, Order sortOrder, int page, int elementsPerPage) {

        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.statuses = statuses;
        this.sortColumn = sortColumn;
        this.sortOrder = sortOrder;
        this.page = page;
        this.elementsPerPage = elementsPerPage;
    }

    public static Builder builder() {
        return new Builder();
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public EnumSet<ExpositionStatus> getStatuses() {
        return statuses;
    }

    public Column getSortColumn() {
        return sortColumn;
    }

    public Order getSortOrder() {
        return sortOrder;
    }

    public int getPage() {
        return page;
    }

    public int getElementsPerPage() {
        return elementsPerPage;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ExpositionFilter.class.getSimpleName() + "[", "]")
                .add("dateFrom=" + dateFrom)
                .add("dateTo=" + dateTo)
                .add("statuses=" + statuses)
                .toString();
    }

    public enum Column {
        TOPIC("topic", "topic"), DATE_FROM("start", "dateFrom"), DATE_TO("finish", "dateTo"), PRICE("ticket_price", "price");

        private final String columnName;
        private final String dtoColumnName;

        Column(String columnName, String dtoColumnName) {
            this.columnName = columnName;
            this.dtoColumnName = dtoColumnName;
        }

        public String getColumnName() {
            return columnName;
        }

        public String getDtoColumnName() {
            return dtoColumnName;
        }
    }

    public enum Order {
        ASCENDING("asc"), DESCENDING("desc");

        private final String sortOrder;

        Order(String sortOrder) {
            this.sortOrder = sortOrder;
        }

        public String getSortOrder() {
            return sortOrder;
        }
    }

    public static class Builder {
        private final EnumSet<ExpositionStatus> statuses = EnumSet.noneOf(ExpositionStatus.class);
        private LocalDate dateFrom = null;
        private LocalDate dateTo = null;
        private Column sortColumn = Column.TOPIC;
        private Order sortOrder = Order.ASCENDING;
        private int page = 1;
        private int elementsPerPage = ELEMENTS_PER_PAGE;

        public Builder addDateFrom(LocalDate dateFrom) {
            this.dateFrom = dateFrom;

            return this;
        }

        public Builder addDateTo(LocalDate dateTo) {
            this.dateTo = dateTo;

            return this;
        }

        public Builder addStatus(ExpositionStatus status) {
            this.statuses.add(status);

            return this;
        }

        public Builder sortBy(Column sortColumn) {
            sortBy(sortColumn, Order.ASCENDING);

            return this;
        }

        public Builder sortBy(Column sortColumn, Order sortOrder) {

            if (sortColumn != null) {
                this.sortColumn = sortColumn;
            }

            if (sortOrder != null) {
                this.sortOrder = sortOrder;
            }

            return this;
        }

        public Builder sortOrder(Order sortOrder) {
            if (sortOrder != null) {
                this.sortOrder = sortOrder;
            }

            return this;
        }

        public Builder addPage(int page) {
            if (page > 0) {
                this.page = page;
            }
            return this;
        }

        public Builder addElementsPerPage(Integer elementsPerPage) {
            if (elementsPerPage > 0) {
                this.elementsPerPage = elementsPerPage;
            }

            return this;
        }

        public ExpositionFilter build() {
            return new ExpositionFilter(dateFrom, dateTo, statuses, sortColumn, sortOrder, page, elementsPerPage);
        }
    }
}
