package org.example.expositions.dao;

import java.util.List;
import java.util.Optional;

public interface DAO<T> {
    Optional<T> save(T entity);
    Optional<T> getById(long id);
    List<T> getAll();
    void update(T entity);
    void delete(long id);
}
