package org.example.expositions.dao;

import org.example.expositions.entity.Schedule;

import java.util.List;

public interface ScheduleDAO extends DAO<Schedule> {
    List<Schedule> getByExpositionId(long expositionId);
}
