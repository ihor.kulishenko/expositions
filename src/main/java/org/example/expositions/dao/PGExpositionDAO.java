package org.example.expositions.dao;

import org.example.expositions.entity.Exposition;
import org.example.expositions.entity.ExpositionStatus;
import org.example.expositions.factory.ExpositionFactory;
import org.example.expositions.util.ExpositionFilter;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of ExpositionDAO for Postgres DB
 */
class PGExpositionDAO extends BaseDAO implements ExpositionDAO {

    public PGExpositionDAO(ConnectionFactory connectionFactory) {
        super(connectionFactory);
    }

    @Override
    public Optional<Exposition> save(Exposition entity) {
        final var sql = "INSERT INTO public.exposition (topic, start, finish, ticket_price, status) " +
                "VALUES (?, ?, ?, ?, ?::public.exposition_status) RETURNING id";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, entity.getTopic());
            ps.setObject(2, entity.getStart());
            ps.setObject(3, entity.getFinish());
            ps.setBigDecimal(4, entity.getTicketPrice());
            ps.setString(5, entity.getStatus().name());

            final var rs = ps.executeQuery();
            if (rs.next()) {
                final var id = rs.getLong("id");
                return Optional.of(new Exposition(id, entity));
            }
        } catch (Exception e) {
            throw new DAOException(e);
        }

        return Optional.empty();
    }

    @Override
    public Optional<Exposition> getById(long id) {
        final var sql = "SELECT id AS exp_id, topic AS exp_topic, start AS exp_start, finish AS exp_finish," +
                "ticket_price AS exp_ticket_price, status AS exp_status " +
                " FROM public.exposition WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, id);

            final var rs = ps.executeQuery();
            if (rs.next()) {
                return Optional.of(ExpositionFactory.createEntity(rs));
            }
        } catch (Exception e) {
            throw new DAOException(e);
        }

        return Optional.empty();
    }

    @Override
    public List<Exposition> getAll() {
        final var sql = "SELECT id AS exp_id, topic AS exp_topic, start AS exp_start, finish AS exp_finish, " +
                "ticket_price AS exp_ticket_price, status AS exp_status " +
                "FROM public.exposition";

        try (final var ps = getConnection().prepareStatement(sql)) {

            final var rs = ps.executeQuery();

            final var result = new ArrayList<Exposition>();
            while (rs.next()) {
                result.add(ExpositionFactory.createEntity(rs));
            }

            return result;
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Exposition> getByFilter(ExpositionFilter filter) {
        final var sql = "SELECT id AS exp_id, topic AS exp_topic, start AS exp_start, finish AS exp_finish, " +
                " ticket_price AS exp_ticket_price, status AS exp_status " +
                " FROM public.exposition WHERE TRUE "
                + (filter.getDateFrom() != null ? " AND start >= ? " : "")
                + (filter.getDateTo() != null ? " AND finish <= ? " : "")
                + (filter.getStatuses() != null && !filter.getStatuses().isEmpty() ? " AND status = ANY(?::public.exposition_status[])" : "")
                + String.format(" ORDER BY %s %s ", filter.getSortColumn().getColumnName(), filter.getSortOrder().getSortOrder());

        try (final var ps = getConnection().prepareStatement(sql)) {
            var index = 1;

            // date from
            if (filter.getDateFrom() != null) {
                ps.setObject(index++, filter.getDateFrom(), Types.DATE);
            }

            // date to
            if (filter.getDateTo() != null) {
                ps.setObject(index++, filter.getDateTo(), Types.DATE);
            }

            // statuses
            if (filter.getStatuses() != null && !filter.getStatuses().isEmpty()) {
                final var statusArray = ps.getConnection().createArrayOf("VARCHAR",
                        filter.getStatuses().toArray(new ExpositionStatus[0]));

                ps.setArray(index, statusArray);
            }

            final var rs = ps.executeQuery();

            final var result = new ArrayList<Exposition>();
            while (rs.next()) {
                result.add(ExpositionFactory.createEntity(rs));
            }

            return result;
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void update(Exposition entity) {
        final var sql = "UPDATE public.exposition SET topic = ?, start = ?, finish = ?, ticket_price = ?, " +
                " status = ?::public.exposition_status " +
                " WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, entity.getTopic());
            ps.setObject(2, entity.getStart());
            ps.setObject(3, entity.getFinish());
            ps.setBigDecimal(4, entity.getTicketPrice());
            ps.setString(5, entity.getStatus().name());
            ps.setLong(6, entity.getId());

            ps.executeUpdate();

        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(long id) {
        final var sql = "DELETE FROM public.exposition WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, id);

            ps.executeUpdate();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void inactivate(long expositionId) {
        final var sql = "UPDATE public.exposition SET status = 'CANCELED' WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, expositionId);

            ps.executeUpdate();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }
}
