package org.example.expositions.dao;

import org.example.expositions.entity.User;
import org.example.expositions.factory.UserFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of UserDAO for Postgres DB
 */
class PGUserDAO extends BaseDAO implements UserDAO {

    public PGUserDAO(ConnectionFactory connectionFactory) {
        super(connectionFactory);
    }

    @Override
    public Optional<User> save(User entity) {

        final var sql = "INSERT INTO public.user (first_name, last_name, login, password, email, role) " +
                " VALUES (?, ?, ?, ?, ?, ?::public.user_role) RETURNING id";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, entity.getFirstName());
            ps.setString(2, entity.getLastName());
            ps.setString(3, entity.getLogin());
            ps.setString(4, entity.getPassword());
            ps.setString(5, entity.getEmail());
            ps.setString(6, entity.getRole().name());

            final var rs = ps.executeQuery();
            if (rs.next()) {
                final var id = rs.getLong("id");
                return Optional.of(new User(id, entity));
            }

            return Optional.empty();

        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Optional<User> getById(long id) {
        final var sql = "SELECT " +
                "id as usr_id, first_name as usr_first_name, last_name as usr_last_name, login as usr_login," +
                "password as usr_password, email as usr_email, role as usr_role" +
                " FROM public.\"user\" WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, id);

            final var rs = ps.executeQuery();
            return rs.next() ? Optional.of(UserFactory.createEntity(rs)) : Optional.empty();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<User> getAll() {
        final var sql = "SELECT " +
                "id as usr_id, first_name as usr_first_name, last_name as usr_last_name, login as usr_login," +
                "password as usr_password, email as usr_email, role as usr_role " +
                "FROM public.\"user\"";

        try (final var ps = getConnection().prepareStatement(sql)) {
            final var rs = ps.executeQuery();

            final var result = new ArrayList<User>();

            while (rs.next()) {
                result.add(UserFactory.createEntity(rs));
            }

            return result;
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Optional<User> getByLogin(String login) {
        final var sql = "SELECT " +
                "id AS usr_id, first_name AS usr_first_name, lASt_name AS usr_last_name, login AS usr_login," +
                "pASsword AS usr_password, email AS usr_email, role AS usr_role " +
                " FROM public.\"user\" WHERE login = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, login);
            final var rs = ps.executeQuery();

            return rs.next() ? Optional.of(UserFactory.createEntity(rs)) : Optional.empty();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void update(User entity) {
        final var sql = "UPDATE public.\"user\" SET first_name = ?, last_name = ?, " +
                "password = ?, email = ?, role = ?::public.user_role WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, entity.getFirstName());
            ps.setString(2, entity.getLastName());
            ps.setString(3, entity.getPassword());
            ps.setString(4, entity.getEmail());
            ps.setString(5, entity.getRole().name());
            ps.setLong(6, entity.getId());

            ps.executeUpdate();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(long id) {
        final var sql = "DELETE FROM public.\"user\" WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, id);

            ps.executeUpdate();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }
}
