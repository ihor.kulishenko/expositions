package org.example.expositions.dao;

import org.example.expositions.entity.User;

import java.util.Optional;

public interface UserDAO extends DAO<User> {
    Optional<User> getByLogin(String login);
}
