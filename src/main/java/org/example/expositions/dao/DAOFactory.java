package org.example.expositions.dao;

/**
 * Factory class that provides specific DAO implementation
 */
public final class DAOFactory {

    private DAOFactory() {

    }

    @SuppressWarnings("unchecked")
    public static <T> T getDAO(Class<T> daoClass, ConnectionFactory connectionFactory) {
        if (daoClass == ExpositionDAO.class) {
            return (T) new PGExpositionDAO(connectionFactory);
        } else if (daoClass == ExpositionHallDAO.class) {
            return (T) new PGExpositionHallDAO(connectionFactory);
        } else if (daoClass == HallDAO.class) {
            return (T) new PGHallDAO(connectionFactory);
        } else if (daoClass == ScheduleDAO.class) {
            return (T) new PGScheduleDAO(connectionFactory);
        } else if (daoClass == TicketDAO.class) {
            return (T) new PGTicketDAO(connectionFactory);
        } else if (daoClass == UserDAO.class) {
            return (T) new PGUserDAO(connectionFactory);
        }

        throw new DAOException("unknown dao class: " + daoClass);
    }
}
