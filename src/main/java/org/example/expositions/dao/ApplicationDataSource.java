package org.example.expositions.dao;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.util.Properties;


/**
 * Data source for pool-based DB connections
 */
public class ApplicationDataSource {
    private static final DataSource dataSource;

    static {
        final var classLoader = Thread.currentThread().getContextClassLoader();
        try (final var inputStream = classLoader.getResourceAsStream("hikari.properties")) {
            final var properties = new Properties();
            properties.load(inputStream);
            dataSource = new HikariDataSource(new HikariConfig(properties));

        } catch (Exception e) {
            throw new AppException(e);
        }
    }

    public static DataSource getDataSource() {
        return dataSource;
    }

    public static void closeDataSource() {
        if (dataSource != null) {
            ((HikariDataSource) dataSource).close();
        }
    }
}
