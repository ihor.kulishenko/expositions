package org.example.expositions.dao;

import org.example.expositions.entity.Exposition;
import org.example.expositions.entity.Hall;

import java.util.List;

public interface ExpositionHallDAO {
    void save(Exposition exposition, Hall hall);
    List<Hall> get(Exposition exposition);
    void delete(Exposition exposition, Hall hall);
}
