package org.example.expositions.dao;

import org.example.expositions.entity.Hall;

import java.util.Optional;

public interface HallDAO extends DAO<Hall> {
    Optional<Hall> getByName(String name);
}
