package org.example.expositions.dao;

/**
 * Parent class for all DAO-based exceptions
 */
public class DAOException extends AppException {
    public DAOException(Throwable cause) {
        super(cause);
    }

    public DAOException(String message) {
        super(message);
    }
}
