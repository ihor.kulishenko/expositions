package org.example.expositions.dao;

import org.example.expositions.entity.Exposition;
import org.example.expositions.util.ExpositionFilter;

import java.util.List;

public interface ExpositionDAO extends DAO<Exposition> {
    List<Exposition> getByFilter(ExpositionFilter expositionFilter);
    void inactivate(long expositionId);
}
