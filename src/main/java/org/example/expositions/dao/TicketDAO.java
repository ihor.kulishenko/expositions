package org.example.expositions.dao;

import org.example.expositions.entity.Ticket;

public interface TicketDAO extends DAO<Ticket> {
    long getTotalNumber(long expositionId);
}
