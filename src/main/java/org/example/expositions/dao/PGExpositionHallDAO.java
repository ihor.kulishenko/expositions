package org.example.expositions.dao;

import org.example.expositions.entity.Exposition;
import org.example.expositions.entity.Hall;
import org.example.expositions.factory.HallFactory;

import java.util.List;


/**
 * Implementation of ExpositionHallDAO for Postgres DB
 */
class PGExpositionHallDAO extends BaseDAO implements ExpositionHallDAO {

    public PGExpositionHallDAO(ConnectionFactory connectionFactory) {
        super(connectionFactory);
    }

    @Override
    public void save(Exposition exposition, Hall hall) {
        final var sql = "INSERT INTO public.exposition_hall (exposition_id, hall_id) VALUES (?, ?)";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, exposition.getId());
            ps.setLong(2, hall.getId());

            ps.executeUpdate();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Hall> get(Exposition exposition) {
        final var sql = "SELECT h.id AS hl_id, h.name AS hl_name" +
                " FROM public.hall h, public.exposition e, public.exposition_hall eh " +
                "WHERE h.id = eh.hall_id AND e.id = eh.exposition_id AND eh.exposition_id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, exposition.getId());

            final var rs = ps.executeQuery();

            return HallFactory.createEntityList(rs);
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(Exposition exposition, Hall hall) {
        final var sql = "DELETE FROM public.exposition_hall WHERE exposition_id = ? AND hall_id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, exposition.getId());
            ps.setLong(2, hall.getId());

            ps.executeUpdate();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }
}
