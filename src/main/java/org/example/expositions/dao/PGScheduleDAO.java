package org.example.expositions.dao;

import org.example.expositions.entity.Schedule;
import org.example.expositions.factory.ScheduleFactory;

import java.util.List;
import java.util.Optional;


/**
 * Implementation of ScheduleDAO for Postgres DB
 */
class PGScheduleDAO extends BaseDAO implements ScheduleDAO {

    public PGScheduleDAO(ConnectionFactory connectionFactory) {
        super(connectionFactory);
    }

    @Override
    public Optional<Schedule> save(Schedule entity) {
        final var sql = "INSERT INTO public.schedule (exposition_id, weekday, start, finish) " +
                " VALUES (?, ?::public.weekday_type, ?, ?) RETURNING id";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, entity.getExpositionId());
            ps.setString(2, entity.getDayOfWeek().name());
            ps.setObject(3, entity.getStart());
            ps.setObject(4, entity.getFinish());

            final var rs = ps.executeQuery();
            if (rs.next()) {
                final var id = rs.getLong("id");
                return Optional.of(new Schedule(id, entity));
            }

            return Optional.empty();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Optional<Schedule> getById(long id) {
        final var sql = "SELECT s.id AS sc_id, s.exposition_id as sc_exposition_id, s.weekday AS sc_weekday, " +
                " s.start AS sc_start, s.finish AS sc_finish " +
                "FROM public.schedule s " +
                " WHERE s.id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, id);

            final var rs = ps.executeQuery();
            if (rs.next()) {
                return Optional.of(ScheduleFactory.createEntity(rs));
            }

            return Optional.empty();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Schedule> getAll() {
        final var sql = "SELECT s.id AS sc_id, s.exposition_id as sc_exposition_id, s.weekday AS sc_weekday, " +
                " s.start AS sc_start, s.finish AS sc_finish " +
                " FROM public.schedule s ";

        try (final var ps = getConnection().prepareStatement(sql)) {
            final var rs = ps.executeQuery();

            return ScheduleFactory.createEntityList(rs);
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Schedule> getByExpositionId(long expositionId) {
        final var sql = "SELECT s.id AS sc_id, s.exposition_id as sc_exposition_id, s.weekday AS sc_weekday, " +
                " s.start AS sc_start, s.finish AS sc_finish " +
                " FROM public.schedule s " +
                " WHERE s.exposition_id =  ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, expositionId);
            final var rs = ps.executeQuery();

            return ScheduleFactory.createEntityList(rs);
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void update(Schedule entity) {
        final var sql = "UPDATE public.schedule set start = ?, finish = ?" +
                "WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setObject(1, entity.getStart());
            ps.setObject(2, entity.getFinish());
            ps.setLong(3, entity.getId());

            ps.executeUpdate();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(long id) {
        final var sql = "DELETE FROM public.schedule WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, id);

            ps.executeUpdate();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }
}
