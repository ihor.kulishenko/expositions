package org.example.expositions.dao;

import org.example.expositions.entity.Ticket;
import org.example.expositions.factory.TicketFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of TicketDAO for Postgres DB
 */
class PGTicketDAO extends BaseDAO implements TicketDAO {

    public PGTicketDAO(ConnectionFactory connectionFactory) {
        super(connectionFactory);
    }

    @Override
    public Optional<Ticket> save(Ticket entity) {
        final var sql = "INSERT INTO public.ticket (user_id, exposition_id, date, price) " +
                "VALUES(?, ?, ?, ?) RETURNING id";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, entity.getUser().getId());
            ps.setLong(2, entity.getExposition().getId());
            ps.setObject(3, entity.getDate());
            ps.setObject(4, entity.getPrice());

            final var rs = ps.executeQuery();

            if (rs.next()) {
                final var id = rs.getLong("id");
                return Optional.of(new Ticket(id, entity));
            }

            return Optional.empty();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Optional<Ticket> getById(long id) {
        final var sql = "SELECT " +
                "t.id AS tck_id, t.date AS tck_date, t.price as tck_price, " +

                "e.id AS exp_id, e.topic AS exp_topic, e.start AS exp_start, e.finish AS exp_finish, " +
                "e.ticket_price AS exp_ticket_price, e.status AS exp_status," +

                "u.id AS usr_id, u.first_name AS usr_first_name, u.lASt_name AS usr_lASt_name, u.login AS usr_login," +
                "u.pASsword AS usr_pASsword, u.email AS usr_email, u.role AS usr_role" +

                " FROM public.ticket t, public.exposition e, public.\"user\" u " +
                "WHERE t.exposition_id = e.id AND t.user_id = u.id AND t.id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, id);

            final var rs = ps.executeQuery();

            if (rs.next()) {
                return Optional.of(TicketFactory.createEntity(rs));
            }

            return Optional.empty();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Ticket> getAll() {
        final var sql = "SELECT " +
                "t.id AS tck_id, t.date AS tck_date, t.price as tck_price, " +

                "e.id AS exp_id, e.topic AS exp_topic, e.start AS exp_start, e.finish AS exp_finish, " +
                "e.ticket_price AS exp_ticket_price, e.status AS exp_status," +

                "u.id AS usr_id, u.first_name AS usr_first_name, u.lASt_name AS usr_lASt_name, u.login AS usr_login," +
                "u.pASsword AS usr_pASsword, u.email AS usr_email, u.role AS usr_role" +

                " FROM public.ticket t, public.exposition e, public.\"user\" u " +
                "WHERE t.exposition_id = e.id AND t.user_id = u.id";

        try (final var ps = getConnection().prepareStatement(sql)) {
            final var rs = ps.executeQuery();

            final var result = new ArrayList<Ticket>();
            while (rs.next()) {
                result.add(TicketFactory.createEntity(rs));
            }

            return result;
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public long getTotalNumber(long expositionId) {
        final var sql = "SELECT count(*) AS total_number FROM public.ticket WHERE exposition_id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, expositionId);

            final var rs = ps.executeQuery();
            return rs.next() ? rs.getLong("total_number") : 0L;
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void update(Ticket entity) {
        final var sql = "UPDATE public.ticket SET user_id = ?, exposition_id = ?, date = ?, price = ? WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, entity.getUser().getId());
            ps.setLong(2, entity.getExposition().getId());
            ps.setObject(3, entity.getDate());
            ps.setBigDecimal(4, entity.getPrice());
            ps.setLong(5, entity.getId());

            ps.executeUpdate();

        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(long id) {
        final var sql = "DELETE FROM public.ticket WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, id);

            ps.executeUpdate();

        } catch (Exception e) {
            throw new DAOException(e);
        }
    }
}
