package org.example.expositions.dao;


import java.sql.Connection;

/**
 * Parent class for DAO classes
 */
public abstract class BaseDAO {
    private final ConnectionFactory connectionFactory;

    public BaseDAO(ConnectionFactory parentConnectionFactory) {
        this.connectionFactory = parentConnectionFactory;
    }

    protected Connection getConnection() {
        return connectionFactory.getConnection();
    }
}
