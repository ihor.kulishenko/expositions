package org.example.expositions.dao;

import org.example.expositions.entity.Hall;
import org.example.expositions.factory.HallFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Implementation of HallDAO for Postgres DB
 */
class PGHallDAO extends BaseDAO implements HallDAO {

    public PGHallDAO(ConnectionFactory connectionFactory) {
        super(connectionFactory);
    }

    @Override
    public Optional<Hall> save(Hall entity) {
        final var sql = "INSERT INTO public.hall (name) VALUES(?) RETURNING id";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, entity.getName());

            final var rs = ps.executeQuery();

            if (rs.next()) {
                final var id = rs.getLong("id");
                return Optional.of(new Hall(id, entity));
            }

        } catch (Exception e) {
            throw new DAOException(e);
        }

        return Optional.empty();
    }

    @Override
    public Optional<Hall> getById(long id) {
        final var sql = "SELECT id AS hl_id, name AS hl_name from public.hall WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, id);
            final var rs = ps.executeQuery();
            if (rs.next()) {
                return Optional.of(HallFactory.createEntity(rs));
            }
        } catch (Exception e) {
            throw new DAOException(e);
        }
        return Optional.empty();
    }

    @Override
    public List<Hall> getAll() {
        final var sql = "SELECT id AS hl_id, name AS hl_name FROM public.hall";

        try (final var ps = getConnection().prepareStatement(sql)) {
            final var rs = ps.executeQuery();

            final var result = new ArrayList<Hall>();
            while (rs.next()) {
                result.add(HallFactory.createEntity(rs));
            }

            return result;
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Optional<Hall> getByName(String name) {
        final var sql = "SELECT id AS hl_id, name AS hl_name FROM public.hall WHERE name = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, name);
            final var rs = ps.executeQuery();

            return rs.next() ? Optional.of(HallFactory.createEntity(rs)) : Optional.empty();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void update(Hall entity) {
        final var sql = "UPDATE public.hall SET name = ? WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setString(1, entity.getName());
            ps.setLong(2, entity.getId());

            ps.executeUpdate();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    @Override
    public void delete(long id) {
        final var sql = "DELETE FROM public.hall WHERE id = ?";

        try (final var ps = getConnection().prepareStatement(sql)) {
            ps.setLong(1, id);

            ps.executeUpdate();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }
}
