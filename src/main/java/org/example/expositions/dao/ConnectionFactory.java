package org.example.expositions.dao;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Helper class for DB connections and transactions
 */
public class ConnectionFactory {
    private final ConnectionFactory parentFactory;
    private boolean inTransaction;
    private boolean error;
    private Connection connectionHolder;

    public ConnectionFactory(ConnectionFactory parentFactory) {
        this.parentFactory = parentFactory;
        this.inTransaction = false;
        this.error = false;
        this.connectionHolder = null;
    }

    public ConnectionFactory() {
        this(null);
    }

    public Connection getConnection() {
        try {
            if (parentFactory != null) {
                return parentFactory.getConnection();
            }

            if (connectionHolder == null) {
                connectionHolder = ApplicationDataSource.getDataSource().getConnection();
            }

            return connectionHolder;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public void closeConnection() {
        if (parentFactory != null || inTransaction) {
            return;
        }

        try {
            if (connectionHolder != null) {
                if (!connectionHolder.isClosed()) {
                    connectionHolder.setAutoCommit(true);
                    connectionHolder.close();
                }
                connectionHolder = null;
                error = false;
            }
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    public void startTransaction() {
        if (parentFactory != null || inTransaction) {
            return;
        }

        try {
            if (connectionHolder != null) {
                if (!connectionHolder.isClosed()) {
                    connectionHolder.setAutoCommit(true);
                    connectionHolder.close();
                }
                connectionHolder = null;
            }

            connectionHolder = ApplicationDataSource.getDataSource().getConnection();
            connectionHolder.setAutoCommit(false);
            inTransaction = true;
            error = false;

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    public void commitTransaction() {
        if (parentFactory != null || !inTransaction || connectionHolder == null) {
            return;
        }

        try {
            if (!error) {
                connectionHolder.commit();
            } else {
                connectionHolder.rollback();
            }
        } catch (Exception e) {
            throw new DAOException(e);
        }

    }

    public void rollbackTransaction() {
        if (parentFactory != null) {
            parentFactory.error = true;
            return;
        }

        try {
            connectionHolder.rollback();
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

    public void finishTransaction() {
        if (parentFactory != null) {
            return;
        }

        try {
            connectionHolder.setAutoCommit(true);
            connectionHolder.close();
            connectionHolder = null;
            inTransaction = false;
            error = false;
        } catch (Exception e) {
            throw new DAOException(e);
        }
    }

}
