package org.example.expositions.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.StringJoiner;

/**
 * Represents ticket entity
 */
public class Ticket {
    private final long id;
    private final User user;
    private final Exposition exposition;
    private final LocalDate date;
    private final BigDecimal price;

    public Ticket(long id, User user, Exposition exposition, LocalDate date, BigDecimal price) {
        this.id = id;
        this.user = user;
        this.exposition = exposition;
        this.date = date;
        this.price = price;
    }

    public Ticket(long id, Ticket entity) {
        this(id, entity.getUser(), entity.getExposition(), entity.getDate(), entity.getPrice());
    }

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Exposition getExposition() {
        return exposition;
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Ticket.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("user=" + user)
                .add("exposition=" + exposition)
                .add("date=" + date)
                .add("price=" + price)
                .toString();
    }
}
