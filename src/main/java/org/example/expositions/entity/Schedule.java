package org.example.expositions.entity;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.StringJoiner;

/**
 * Represents schedule entity
 */
public class Schedule {
    private final long id;
    private final long expositionId;
    private final DayOfWeek dayOfWeek;
    private final LocalTime start;
    private final LocalTime finish;

    public Schedule(long id, long expositionId, DayOfWeek dayOfWeek, LocalTime start, LocalTime finish) {
        this.id = id;
        this.expositionId = expositionId;
        this.dayOfWeek = dayOfWeek;
        this.start = start;
        this.finish = finish;
    }

    public Schedule(long id, Schedule entity) {
        this(id, entity.getExpositionId(), entity.getDayOfWeek(), entity.getStart(), entity.getFinish());
    }

    public long getId() {
        return id;
    }

    public long getExpositionId() {
        return expositionId;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public LocalTime getStart() {
        return start;
    }

    public LocalTime getFinish() {
        return finish;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Schedule.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("expositionId=" + expositionId)
                .add("dayOfWeek=" + dayOfWeek)
                .add("start=" + start)
                .add("finish=" + finish)
                .toString();
    }
}
