package org.example.expositions.entity;

/**
 * Possible exposition statuses
 */
public enum ExpositionStatus {
    ACTIVE, CANCELED
}
