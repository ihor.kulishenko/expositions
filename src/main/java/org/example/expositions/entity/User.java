package org.example.expositions.entity;


import java.util.StringJoiner;

/**
 * Represents user entity
 */
public class User {
    private final long id;
    private final String firstName;
    private final String lastName;
    private final String login;
    private final String password;
    private final String email;
    private final Role role;

    public User(long id, String firstName, String lastName, String login, String password, String email, Role role) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.password = password;
        this.email = email;
        this.role = role;
    }

    public User(long id, User user) {
        this(id, user.getFirstName(), user.getLastName(), user.getLogin(), user.getPassword(), user.getEmail(),
                user.getRole());
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("login='" + login + "'")
                .add("password='" + password + "'")
                .add("email='" + email + "'")
                .add("role=" + role)
                .toString();
    }
}
