package org.example.expositions.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.StringJoiner;

/**
 * Represents Exposition entity
 */
public class Exposition {
    private final long id;
    private final String topic;
    private final LocalDate start;
    private final LocalDate finish;
    private final BigDecimal ticketPrice;
    private final ExpositionStatus status;

    public Exposition(long id, String topic, LocalDate start, LocalDate finish, BigDecimal ticketPrice,
                      ExpositionStatus status) {
        this.id = id;
        this.topic = topic;
        this.start = start;
        this.finish = finish;
        this.ticketPrice = ticketPrice;
        this.status = status;
    }

    public Exposition(long id, Exposition entity) {
        this(id, entity.getTopic(), entity.getStart(), entity.getFinish(), entity.getTicketPrice(), entity.getStatus());
    }

    public long getId() {
        return id;
    }

    public String getTopic() {
        return topic;
    }

    public LocalDate getStart() {
        return start;
    }

    public LocalDate getFinish() {
        return finish;
    }

    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public ExpositionStatus getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Exposition.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("topic='" + topic + "'")
                .add("start=" + start)
                .add("finish=" + finish)
                .add("ticketPrice=" + ticketPrice)
                .add("status=" + status)
                .toString();
    }
}
