package org.example.expositions.entity;

import java.util.StringJoiner;

/**
 * Represents Hall entity
 */
public class Hall {
    private final long id;
    private final String name;

    public Hall(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Hall(long id, Hall entity) {
        this(id, entity.getName());
    }

    public long getId() {
        return id;
    }


    public String getName() {
        return name;
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", Hall.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .toString();
    }
}
