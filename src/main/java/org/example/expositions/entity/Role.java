package org.example.expositions.entity;

/**
 * Possible user roles
 */
public enum Role {
    ANONYMOUS, USER, ADMINISTRATOR
}
