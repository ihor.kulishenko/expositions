package org.example.expositions.dto;

import org.example.expositions.util.ExpositionFilter;

import java.time.LocalDate;
import java.util.StringJoiner;

public class ExpositionFilterDTO {
    private final LocalDate dateFrom;
    private final LocalDate dateTo;
    private final ExpositionFilter.Column sortColumn;
    private final ExpositionFilter.Order sortOrder;
    private final Integer page;
    private final Integer elementsPerPage;

    public ExpositionFilterDTO(LocalDate dateFrom, LocalDate dateTo, ExpositionFilter.Column sortColumn,
                               ExpositionFilter.Order sortOrder, Integer page, Integer elementsPerPage) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.sortColumn = sortColumn;
        this.sortOrder = sortOrder;
        this.page = page;
        this.elementsPerPage = elementsPerPage;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public ExpositionFilter.Column getSortColumn() {
        return sortColumn;
    }

    public ExpositionFilter.Order getSortOrder() {
        return sortOrder;
    }

    public Integer getPage() {
        return page;
    }

    public Integer getElementsPerPage() {
        return elementsPerPage;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ExpositionFilterDTO.class.getSimpleName() + "[", "]")
                .add("dateFrom=" + dateFrom)
                .add("dateTo=" + dateTo)
                .add("sortColumn=" + sortColumn)
                .add("sortOrder=" + sortOrder)
                .add("page=" + page)
                .add("elementsPerPage=" + elementsPerPage)
                .toString();
    }
}
