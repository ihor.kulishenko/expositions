package org.example.expositions.dto;

import org.example.expositions.util.ExpositionFilter;

import java.time.LocalDate;
import java.util.List;
import java.util.StringJoiner;

public class FilteredExpositionsDTO {
    private final LocalDate dateFrom;
    private final LocalDate dateTo;
    private final List<ExpositionDTO> expositions;
    private final String sortColumn;
    private final String sortOrder;
    private final long currentPage;
    private final long totalPages;
    private final long elementsPerPage;

    public FilteredExpositionsDTO(LocalDate dateFrom, LocalDate dateTo, List<ExpositionDTO> expositions,
                                  String sortColumn, String sortOrder,
                                  long currentPage, long totalPages, long elementsPerPage) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.expositions = expositions;
        this.sortColumn = sortColumn;
        this.sortOrder = sortOrder;
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.elementsPerPage = elementsPerPage;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public List<ExpositionDTO> getExpositions() {
        return expositions;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public long getElementsPerPage() {
        return elementsPerPage;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", FilteredExpositionsDTO.class.getSimpleName() + "[", "]")
                .add("dateFrom=" + dateFrom)
                .add("dateTo=" + dateTo)
                .add("expositions=" + expositions)
                .add("sortColumn=" + sortColumn)
                .add("sortOrder=" + sortOrder)
                .add("currentPage=" + currentPage)
                .add("totalPages=" + totalPages)
                .add("elementsPerPage=" + elementsPerPage)
                .toString();
    }
}
