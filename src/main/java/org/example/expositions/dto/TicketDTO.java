package org.example.expositions.dto;

import java.time.LocalDate;

/**
 * Uses to communicate with web-clients
 */
public class TicketDTO {
    private long expositionId;
    private LocalDate date;

    public long getExpositionId() {
        return expositionId;
    }

    public void setExpositionId(long expositionId) {
        this.expositionId = expositionId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
