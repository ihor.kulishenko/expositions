package org.example.expositions.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalTime;
import java.util.StringJoiner;

/**
 * Uses to communicate with web-clients
 */
public class ScheduleDTO {
    @JsonProperty("weekday")
    private final String weekdayCode;
    private final String lcWeekday;
    private final LocalTime timeFrom;
    private final LocalTime timeTo;

    public ScheduleDTO(String weekdayCode, LocalTime timeFrom, LocalTime timeTo) {
        this(weekdayCode, weekdayCode, timeFrom, timeTo);
    }

    public ScheduleDTO(String weekdayCode, String lcWeekday, LocalTime timeFrom, LocalTime timeTo) {
        this.weekdayCode = weekdayCode;
        this.lcWeekday = lcWeekday;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
    }

    public String getWeekdayCode() {
        return weekdayCode;
    }

    public String getLcWeekday() {
        return lcWeekday;
    }

    public LocalTime getTimeFrom() {
        return timeFrom;
    }

    public LocalTime getTimeTo() {
        return timeTo;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ScheduleDTO.class.getSimpleName() + "[", "]")
                .add("weekdayCode='" + weekdayCode + "'")
                .add("timeFrom=" + timeFrom)
                .add("timeTo=" + timeTo)
                .toString();
    }
}
