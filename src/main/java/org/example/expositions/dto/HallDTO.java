package org.example.expositions.dto;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.StringJoiner;

/**
 * Uses to communicate with web-clients
 */
public class HallDTO {
    private long id;
    private String name;

    public HallDTO(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @JsonCreator
    public HallDTO(long id) {
        this(id, null);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", HallDTO.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .toString();
    }
}
