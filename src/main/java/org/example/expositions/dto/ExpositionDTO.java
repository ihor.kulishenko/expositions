package org.example.expositions.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.StringJoiner;

/**
 * Uses to communicate with web-clients
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExpositionDTO {
    private Long id;
    private String topic;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private List<HallDTO> halls;
    @JsonProperty("price")
    private BigDecimal ticketPrice;
    private List<ScheduleDTO> schedule;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public List<HallDTO> getHalls() {
        return halls;
    }

    public void setHalls(List<HallDTO> halls) {
        this.halls = halls;
    }

    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public List<ScheduleDTO> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<ScheduleDTO> schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ExpositionDTO.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("topic='" + topic + "'")
                .add("dateFrom=" + dateFrom)
                .add("dateTo=" + dateTo)
                .add("halls=" + halls)
                .add("ticketPrice=" + ticketPrice)
                .add("schedule=" + schedule)
                .toString();
    }
}
