package org.example.expositions.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Uses to communicate with web-clients
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultDTO {
    private final Status status;
    private final String errorMessage;
    private final Object data;

    private ResultDTO(Status status, String errorMessage, Object data) {
        this.status = status;
        this.errorMessage = errorMessage;
        this.data = data;
    }

    public static ResultDTO ok(Object data) {
        return new ResultDTO(Status.OK, null, data);
    }

    public static ResultDTO failure(String errorMessage) {
        return new ResultDTO(Status.ERROR, errorMessage, null);
    }

    public Status getStatus() {
        return status;
    }


    public String getErrorMessage() {
        return errorMessage;
    }


    public Object getData() {
        return data;
    }

    public enum Status {OK, ERROR}
}
