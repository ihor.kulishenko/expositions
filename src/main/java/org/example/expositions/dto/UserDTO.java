package org.example.expositions.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.StringJoiner;

public class UserDTO {
    private final String firstName;
    private final String lastName;
    private final String login;
    private final String email;

    private final boolean isAdmin;

    public UserDTO(String firstName, String lastName, String login, String email, boolean isAdmin) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.email = email;
        this.isAdmin = isAdmin;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }

    @JsonProperty("isAdmin")
    public boolean isAdmin() {
        return isAdmin;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", UserDTO.class.getSimpleName() + "[", "]")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("login='" + login + "'")
                .add("email='" + email + "'")
                .add("isAdmin=" + isAdmin)
                .toString();
    }
}
