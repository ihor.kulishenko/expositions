package org.example.expositions.delegate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.dao.DAOException;
import org.example.expositions.dao.DAOFactory;
import org.example.expositions.dao.ExpositionDAO;
import org.example.expositions.dao.ExpositionHallDAO;
import org.example.expositions.entity.*;
import org.example.expositions.util.ExpositionFilter;
import org.example.expositions.util.FilteredExposition;
import org.example.expositions.util.Paginator;
import org.example.expositions.util.Validator;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Holds business logic for expositions
 */
public class ExpositionDelegate extends BaseDelegate {
    private static final Logger logger = LogManager.getLogger();

    private final ExpositionDAO expositionDAO;
    private final ExpositionHallDAO expositionHallDAO;
    private final ScheduleDelegate scheduleDelegate;

    public ExpositionDelegate() {
        this(null);
    }

    public ExpositionDelegate(BaseDelegate parentDelegate) {
        super(parentDelegate);
        expositionDAO = DAOFactory.getDAO(ExpositionDAO.class, getConnectionFactory());
        expositionHallDAO = DAOFactory.getDAO(ExpositionHallDAO.class, getConnectionFactory());
//        scheduleDAO = DAOFactory.getDAO(ScheduleDAO.class, getConnectionFactory());
        scheduleDelegate = new ScheduleDelegate(this);
    }

    public Result<Exposition> create(Exposition exposition, List<Hall> halls, List<Schedule> schedule, User user) {
        final var validationResult = createValidation(exposition, halls, schedule, user);

        if (!validationResult.success) {
            logger.error(validationResult.errorMessage);
            return Result.failure(validationResult.errorMessage);
        }

        try {
            super.startTransaction();

            // save exposition
            final var savedExposition = expositionDAO.save(exposition);
            if (savedExposition.isEmpty()) {
                super.rollbackTransaction();
                return Result.failure("error while saving exposition");
            }

            // save halls in exposition_halls
            for (Hall hall : halls) {
                expositionHallDAO.save(savedExposition.get(), hall);
            }

            // save schedule
            final var scheduleResult = scheduleDelegate.save(schedule.stream()
                    .map(_s -> new Schedule(0L, savedExposition.get().getId(), _s.getDayOfWeek(),
                            _s.getStart(), _s.getFinish()))
                    .collect(Collectors.toList()));

            if (scheduleResult.failure()) {
                super.rollbackTransaction();
                logger.error(scheduleResult.message);
                return Result.failure("error while saving schedule");
            }

            super.commitTransaction();

            logger.info("EXPOSITION CREATED: {}", savedExposition.get());

            return Result.ok(savedExposition.get());

        } catch (Exception e) {
            super.rollbackTransaction();
            logger.error(e);
            return Result.failure(e.getMessage());
        } finally {
            super.finishTransaction();
        }
    }

    public Result<Exposition> getById(long expositionId) {
        try {
            final var expositionResult = expositionDAO.getById(expositionId);
            if (expositionResult.isEmpty()) {
                return Result.failure(String.format("exposition with id: %d not found", expositionId));
            } else {
                return Result.ok(expositionResult.get());
            }
        } catch (DAOException e) {
            logger.error(e.getMessage(), e);
            return Result.failure(e.getMessage());
        } finally {
            super.closeConnection();
        }
    }

    public Result<FilteredExposition> getByFilter(ExpositionFilter expositionFilter) {
        try {
            if (expositionFilter == null) {
                return Result.failure("exposition filter isn't set");
            }

            final var expositionResult = expositionDAO.getByFilter(expositionFilter);
            final var paginator = new Paginator(expositionResult.size(), expositionFilter.getElementsPerPage());
            paginator.setCurrentPage(expositionFilter.getPage());

            return Result.ok(new FilteredExposition(expositionFilter.getDateFrom(), expositionFilter.getDateTo(),
                    paginator.getElements(expositionResult), expositionFilter.getSortColumn(), expositionFilter.getSortOrder(),
                    expositionFilter.getPage(), paginator.getTotalPages(), expositionFilter.getElementsPerPage()));

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.failure(e.getMessage());
        } finally {
            super.closeConnection();
        }
    }

    public Result<Boolean> inactivate(long expositionId) {
        try {
            super.startTransaction();
            expositionDAO.inactivate(expositionId);
            super.commitTransaction();

            logger.info("EXPOSITION REMOVED: {}", expositionId);

            return Result.ok(Boolean.TRUE);
        } catch (DAOException e) {
            super.rollbackTransaction();
            logger.error(e.getMessage(), e);
            return Result.failure(e.getMessage());
        } finally {
            super.finishTransaction();
        }
    }

    private Validator.Result createValidation(Exposition exposition, List<Hall> halls, List<Schedule> schedule, User user) {
        final var expositionResult = validateExposition(exposition);
        if (!expositionResult.success) {
            return expositionResult;
        }

        final var hallsResult = validateHalls(halls);
        if (!hallsResult.success) {
            return hallsResult;
        }

        final var scheduleResult = validateSchedule(exposition, schedule);
        if (!scheduleResult.success) {
            return scheduleResult;
        }

        final var userResult = validateUser(user);
        if (!userResult.success) {
            return userResult;
        }

        return Validator.OK;
    }

    private Validator.Result validateExposition(Exposition exposition) {
        return new Validator<Exposition>()
                .add(Validator.nonNullField(Exposition::getTopic), "topic field is not set")
                .add(Validator.nonEmptyField(Exposition::getTopic), "topic field is empty")
                .add(Validator.dateRangeFields(Exposition::getStart, Exposition::getFinish), "incorrect start and finish fields")
                .add(Validator.nonNegativeField(Exposition::getTicketPrice), "incorrect ticket price field")
                .add(_e -> Objects.equals(_e.getStatus(), ExpositionStatus.ACTIVE), "incorrect status field")
                .validate(exposition);
    }

    private Validator.Result validateHalls(List<Hall> halls) {
        return new Validator<List<Hall>>()
                .add(Validator.emptyListField(Function.identity()), "halls not selected")
                .add(Validator.uniqueValueInList(Hall::getId), "non unique halls element")
                .validate(halls);
    }

    private Validator.Result validateSchedule(Exposition exposition, List<Schedule> schedule) {
        final var validateResult = new Validator<List<Schedule>>()
                .add(Validator.emptyListField(Function.identity()), "incorrect schedule")
                .add(Validator.uniqueValueInList(Schedule::getDayOfWeek), "non unique schedule element")
                .validate(schedule);
        if (!validateResult.success) {
            return validateResult;
        }

        //schedule check (have at least one day, doesn't have days that not exists in exposition)
        final var days = exposition.getStart().datesUntil(exposition.getFinish().plusDays(1))
                .map(LocalDate::getDayOfWeek)
                .collect(Collectors.toSet());
        for (Schedule day : schedule) {
            if (!days.contains(day.getDayOfWeek())) {
                return new Validator.Result(false, "day not in the range");
            }
        }

        // validate timeFrom timeTo
        for (Schedule day : schedule) {
            if (day.getStart().isAfter(day.getFinish())) {
                return new Validator.Result(false, "wrong time range");
            }
        }

        return Validator.OK;
    }

    private Validator.Result validateUser(User user) {
        return Optional.ofNullable(user)
                .map(User::getRole)
                .filter(_r -> Objects.equals(_r, Role.ADMINISTRATOR))
                .map(_r -> Validator.OK)
                .orElse(new Validator.Result(false, ""));
    }
}
