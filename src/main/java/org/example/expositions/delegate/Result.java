package org.example.expositions.delegate;

/**
 * Holds returned value if the execution was successful or error reason if not
 *
 * @param <T> - container type
 */
public class Result<T> {
    public final long errorCode;
    public final String message;
    public final T container;
    private final boolean status;

    private Result(boolean status, long errorCode, String message, T object) {
        this.status = status;
        this.errorCode = errorCode;
        this.message = message;
        this.container = object;
    }

    public static <T> Result<T> ok(T object) {
        return new Result<>(true, 0L, "", object);
    }

    public static <T> Result<T> failure(long errorCode, String message) {
        return new Result<>(false, errorCode, message, null);
    }

    public static <T> Result<T> failure(String message) {
        return new Result<>(false, 0L, message, null);
    }

    public boolean success() {
        return status;
    }

    public boolean failure() {
        return !status;
    }
}
