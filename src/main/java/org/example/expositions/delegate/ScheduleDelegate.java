package org.example.expositions.delegate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.dao.DAOFactory;
import org.example.expositions.dao.ScheduleDAO;
import org.example.expositions.entity.Exposition;
import org.example.expositions.entity.Schedule;

import java.util.ArrayList;
import java.util.List;


/**
 * Holds business logic for exposition's schedule
 */
public class ScheduleDelegate extends BaseDelegate {
    private static final Logger logger = LogManager.getLogger();

    private final ScheduleDAO scheduleDAO;

    public ScheduleDelegate() {
        this(null);
    }

    public ScheduleDelegate(BaseDelegate parentDelegate) {
        super(parentDelegate);

        this.scheduleDAO = DAOFactory.getDAO(ScheduleDAO.class, getConnectionFactory());
    }

    public Result<List<Schedule>> save(List<Schedule> scheduleList) {
        try {
            final var savedSchedule = new ArrayList<Schedule>();
            super.startTransaction();

            for (Schedule schedule : scheduleList) {
                final var result = scheduleDAO.save(schedule);
                if (result.isEmpty()) {
                    super.rollbackTransaction();
                    return Result.failure("result is empty");
                }
                savedSchedule.add(result.get());
            }
            super.finishTransaction();
            return Result.ok(savedSchedule);
        } catch (Exception e) {
            super.rollbackTransaction();
            logger.error(e.getMessage(), e);
            return Result.failure(e.getMessage());
        } finally {
            super.closeConnection();
        }
    }

    public Result<List<Schedule>> getByExposition(Exposition exposition) {
        if (exposition == null) {
            return Result.failure("exposition is not set");
        }

        try {
            return Result.ok(scheduleDAO.getByExpositionId(exposition.getId()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.failure(e.getMessage());
        } finally {
            super.closeConnection();
        }
    }
}
