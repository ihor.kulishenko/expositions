package org.example.expositions.delegate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.dao.DAOException;
import org.example.expositions.dao.DAOFactory;
import org.example.expositions.dao.TicketDAO;
import org.example.expositions.entity.Role;
import org.example.expositions.entity.Ticket;
import org.example.expositions.util.Validator;

import java.util.function.Function;

/**
 * Holds business logic for sold tickets
 */
public class TicketDelegate extends BaseDelegate {
    private static final Logger logger = LogManager.getLogger();

    private final TicketDAO ticketDAO;

    public TicketDelegate() {
        this(null);
    }

    public TicketDelegate(BaseDelegate parentDelegate) {
        super(parentDelegate);

        ticketDAO = DAOFactory.getDAO(TicketDAO.class, getConnectionFactory());
    }

    public Result<Ticket> create(Ticket ticket) {
        final var validationResult = createValidation(ticket);
        if (!validationResult.success) {
            logger.error(validationResult.errorMessage);
            return Result.failure(validationResult.errorMessage);
        }

        try {
            super.startTransaction();

            final var savedTicket = ticketDAO.save(updatePrice(ticket));
            if (savedTicket.isEmpty()) {
                super.rollbackTransaction();
                return Result.failure("error saving ticket");
            }

            super.commitTransaction();

            logger.info("TICKET CREATED: {}", savedTicket.get());
            return Result.ok(savedTicket.get());
        } catch (Exception e) {
            super.rollbackTransaction();
            logger.error(e.getMessage(), e);

            return Result.failure(e.getMessage());
        } finally {
            super.finishTransaction();
        }
    }

    public Result<Long> getTotalNumber(long expositionId) {
        try {
            final var totalNumber = ticketDAO.getTotalNumber(expositionId);
            return Result.ok(totalNumber);
        } catch (DAOException e) {
            logger.error(e.getMessage(), e);
            return Result.failure(e.getMessage());
        } finally {
            super.closeConnection();
        }
    }

    private Validator.Result createValidation(Ticket ticket) {
        return new Validator<Ticket>()
                .add(Validator.nonNullField(Function.identity()), "ticket is not set")
                .add(Validator.nonNullField(Ticket::getExposition), "exposition is not set")
                .add(Validator.nonNullField(Ticket::getUser), "user is not set")
                .add(Validator.nonNullField(Ticket::getDate), "date is not set")
                .add(this::checkDateRange, "ticket date not in range")
                .add(this::checkUserPermission, "user not authorized for this operation")
                .validate(ticket);
    }

    private boolean checkDateRange(Ticket ticket) {
        final var dateFrom = ticket.getExposition().getStart();
        final var dateTo = ticket.getExposition().getFinish();
        final var currentDate = ticket.getDate();

        return (currentDate.isAfter(dateFrom) || currentDate.isEqual(dateFrom))
                && (currentDate.isBefore(dateTo) || currentDate.isEqual(dateTo));
    }

    private boolean checkUserPermission(Ticket ticket) {
        final var role = ticket.getUser().getRole();
        return role == Role.USER || role == Role.ADMINISTRATOR;
    }

    private Ticket updatePrice(Ticket ticket) {
        return new Ticket(ticket.getId(), ticket.getUser(), ticket.getExposition(), ticket.getDate(),
                ticket.getExposition().getTicketPrice());
    }
}
