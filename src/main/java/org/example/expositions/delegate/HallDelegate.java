package org.example.expositions.delegate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.dao.DAOFactory;
import org.example.expositions.dao.ExpositionHallDAO;
import org.example.expositions.dao.HallDAO;
import org.example.expositions.entity.Exposition;
import org.example.expositions.entity.Hall;
import org.example.expositions.util.Validator;

import java.util.List;
import java.util.function.Function;


/**
 * Holds business logic for halls
 */
public class HallDelegate extends BaseDelegate {
    private static final Logger logger = LogManager.getLogger();

    private final HallDAO hallDAO;
    private final ExpositionHallDAO expositionHallDAO;

    public HallDelegate() {
        this(null);
    }

    public HallDelegate(BaseDelegate parentDelegate) {
        super(parentDelegate);

        this.hallDAO = DAOFactory.getDAO(HallDAO.class, getConnectionFactory());
        this.expositionHallDAO = DAOFactory.getDAO(ExpositionHallDAO.class, getConnectionFactory());
    }

    public Result<Hall> create(Hall hall) {
        final var validationResult = createValidation(hall);
        if (!validationResult.success) {
            logger.error(validationResult.errorMessage);
            return Result.failure(validationResult.errorMessage);
        }

        try {
            getConnectionFactory().startTransaction();

            final var savedHall = hallDAO.save(hall);
            if (savedHall.isEmpty()) {
                getConnectionFactory().rollbackTransaction();
                return Result.failure("can't save hall");
            }

            super.commitTransaction();

            logger.info("HALL CREATED: {}", savedHall.get().getName());

            return Result.ok(savedHall.get());
        } catch (Exception e) {
            super.rollbackTransaction();
            logger.error(e.getMessage(), e);

            return Result.failure(e.getMessage());
        } finally {
            super.finishTransaction();
        }
    }

    public Result<List<Hall>> getByExposition(Exposition exposition) {
        if (exposition == null) {
            return Result.failure("exposition is not set");
        }

        try {
            return Result.ok(expositionHallDAO.get(exposition));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.failure(e.getMessage());
        } finally {
            super.closeConnection();
        }
    }

    public Result<List<Hall>> getAll() {
        try {
            return Result.ok(hallDAO.getAll());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.failure(e.getMessage());
        } finally {
            super.closeConnection();
        }
    }

    private Validator.Result createValidation(Hall hall) {
        return new Validator<Hall>()
                .add(Validator.nonNullField(Function.identity()), "hall is not set")
                .add(Validator.nonNullField(Hall::getName), "hall name is not seet")
                .add(this::isHallAlreadyExists, "hall name already exists")
                .validate(hall);
    }

    private boolean isHallAlreadyExists(Hall hall) {
        return hallDAO.getByName(hall.getName()).isEmpty();
    }
}
