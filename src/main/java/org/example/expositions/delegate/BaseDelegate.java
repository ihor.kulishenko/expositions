package org.example.expositions.delegate;

import org.example.expositions.dao.ConnectionFactory;

/**
 * Parent class for Delegate classes
 */
public abstract class BaseDelegate {
    private final ConnectionFactory connectionFactory;

    protected BaseDelegate(BaseDelegate parentDelegate) {
        if (parentDelegate == null) {
            this.connectionFactory = new ConnectionFactory();
        } else {
            this.connectionFactory = new ConnectionFactory(parentDelegate.connectionFactory);
        }
    }

    protected ConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    protected void startTransaction() {
        connectionFactory.startTransaction();
    }

    protected void commitTransaction() {
        connectionFactory.commitTransaction();
    }

    protected void rollbackTransaction() {
        connectionFactory.rollbackTransaction();
    }

    protected void finishTransaction() {
        connectionFactory.finishTransaction();
    }

    protected void closeConnection() {
        connectionFactory.closeConnection();
    }
}
