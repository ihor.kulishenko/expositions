package org.example.expositions.delegate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.dao.DAOFactory;
import org.example.expositions.dao.UserDAO;
import org.example.expositions.dto.RegisterDTO;
import org.example.expositions.entity.Role;
import org.example.expositions.entity.User;
import org.example.expositions.util.PasswordUtils;
import org.example.expositions.util.Validator;

import java.util.Objects;
import java.util.function.Function;

/**
 * Holds business logic for application users
 */
public class UserDelegate extends BaseDelegate {
    private static final Logger logger = LogManager.getLogger();

    private final UserDAO userDAO;

    public UserDelegate() {
        this(null);
    }

    public UserDelegate(BaseDelegate parentDelegate) {
        super(parentDelegate);

        userDAO = DAOFactory.getDAO(UserDAO.class, getConnectionFactory());
    }

    public Result<User> create(User user) {
        try {
            super.startTransaction();

            final var validationResult = createValidation(user);
            if (!validationResult.success) {
                logger.error(validationResult.errorMessage);
                return Result.failure(validationResult.errorMessage);
            }

            final var newUser = new User(0L, user.getFirstName(), user.getLastName(),
                    user.getLogin(), PasswordUtils.encode(user.getPassword()), user.getEmail(), user.getRole());

            final var savedUser = userDAO.save(newUser);

            if (savedUser.isEmpty()) {
                super.rollbackTransaction();
                return Result.failure("error creating user");
            }
            super.commitTransaction();

            logger.info("USER CREATED: {}", savedUser.get());

            return Result.ok(savedUser.get());

        } catch (Exception e) {
            super.rollbackTransaction();
            logger.error(e.getMessage(), e);
            return Result.failure(e.getMessage());
        } finally {
            super.finishTransaction();
        }
    }

    public Result<User> create(RegisterDTO dto) {
        return create(dto.getFirstname(), dto.getLastname(), dto.getLogin(), dto.getEmail(),
                dto.getPassword(), dto.getPassword2());
    }

    public Result<User> create(String firstname, String lastname, String login, String email, String password, String password2) {
        if (!Objects.equals(password, password2)) {
            return Result.failure("passwords not equal");
        }

        return create(new User(0L, firstname, lastname, login, password, email, Role.USER));
    }

    public Result<User> getById(long userId) {
        try {
            return userDAO.getById(userId)
                    .map(Result::ok)
                    .orElse(Result.failure("user not found"));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.failure(e.getMessage());
        } finally {
            super.closeConnection();
        }
    }

    public Result<User> checkUser(String username, String plainPassword) {
        try {
            final var validationResult = checkUserValidation(username, plainPassword);
            if (!validationResult.success) {
                return Result.failure(validationResult.errorMessage);
            }

            final var user = userDAO.getByLogin(username);
            if (user.isEmpty()) {
                return Result.failure("cant find user: " + username);
            }

            final var passwordsMatch = PasswordUtils.check(plainPassword, user.get().getPassword());

            return passwordsMatch ? Result.ok(user.get()) : Result.failure("incorrect password");

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.failure("something went wrong");

        } finally {
            super.closeConnection();
        }
    }

    private Validator.Result checkUserValidation(String username, String plainPassword) {
        final var usernameResult = new Validator<String>()
                .add(Validator.nonNullField(Function.identity()), "username is not set")
                .add(Validator.nonEmptyField(Function.identity()), "username is not set")
                .validate(username);

        if (!usernameResult.success) {
            return usernameResult;
        }

        return new Validator<String>()
                .add(Validator.nonNullField(Function.identity()), "password is not set")
                .add(Validator.nonEmptyField(Function.identity()), "password is empty")
                .validate(plainPassword);
    }

    private Validator.Result createValidation(User user) {
        return new Validator<User>()
                .add(Validator.nonNullField(Function.identity()), "user is not set")
                .add(Validator.nonEmptyField(User::getFirstName), "incorrect first name field")
                .add(Validator.nonEmptyField(User::getLastName), "incorrect last name field")
                .add(Validator.nonEmptyField(User::getLogin), "incorrect login field")
                .add(Validator.nonEmptyField(User::getPassword), "incorrect password field")
                .add(Validator.nonEmptyField(User::getEmail), "incorrect email field")
                .add(Validator.nonNullField(User::getRole), "incorrect role field")
                .add(this::checkUserLogin, String.format("user with login %s already exists", user.getLogin()))
                .validate(user);
    }

    private boolean checkUserLogin(User user) {
        return userDAO.getByLogin(user.getLogin()).isEmpty();
    }
}
