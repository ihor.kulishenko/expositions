package org.example.expositions.web.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.dao.ApplicationDataSource;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Application initializer
 */
public class ContextListener implements ServletContextListener {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContextListener.super.contextInitialized(sce);

        logger.info("APPLICATION STARTED");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContextListener.super.contextDestroyed(sce);

        ApplicationDataSource.closeDataSource();

        logger.info("APPLICATION STOPPED{}", System.lineSeparator());
    }
}
