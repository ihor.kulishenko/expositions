package org.example.expositions.web.filter;

import org.example.expositions.util.SessionUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Sets language attribute in requests
 */
public class LanguageFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        final var session = ((HttpServletRequest) request).getSession(true);
        final var userLocale = session.getAttribute(SessionUtils.ATTRIBUTES.currentLanguage.name());
        if (userLocale == null) {
            session.setAttribute(SessionUtils.ATTRIBUTES.currentLanguage.name(), SessionUtils.DEFAULT_LANGUAGE);
        }

        chain.doFilter(request, response);
    }
}
