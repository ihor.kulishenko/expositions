package org.example.expositions.web.tag;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.util.SessionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Prints out localized week's day representation on JSP page
 */
public class WeekdayTag extends TagSupport {
    private static final Logger logger = LogManager.getLogger();

    private String code;

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public int doStartTag() {
        try {
            final var request = (HttpServletRequest) pageContext.getRequest();
            final var language = SessionUtils.getLanguage(request);
            final var resourceBundle = ResourceBundle.getBundle("messages", new Locale(language));

            final var writer = pageContext.getOut();

            writer.print(resourceBundle.getString(code));
        } catch (Exception e) {
            logger.error(e);
        }

        return SKIP_BODY;
    }
}
