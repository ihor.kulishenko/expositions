package org.example.expositions.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.util.SessionUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TemplateServlet extends HttpServlet {
    private static final Logger logger = LogManager.getLogger();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final var pathInfo = request.getPathInfo();
        final var session = request.getSession(false);
        final var currentUser = SessionUtils.getCurrentUser(request);

        logger.info("path info: " + pathInfo);

        if ("/paginator".equals(pathInfo)) {
            request.getRequestDispatcher("/WEB-INF/jsp/template/paginator.jsp").forward(request, response);

        } else if ("/login".equals(pathInfo)) {
            if (currentUser != null) {
                response.sendRedirect(request.getContextPath());
            } else {
                request.getRequestDispatcher("/WEB-INF/jsp/template/login.jsp").forward(request, response);
            }

        } else if ("/register".equals(pathInfo)) {
            request.getRequestDispatcher("/WEB-INF/jsp/template/register.jsp").forward(request, response);

        } else if ("/logout".equals(pathInfo)) {
            if (session != null) {
                session.invalidate();
            }
            response.sendRedirect(request.getContextPath());

        } else if ("/newExposition".equals(pathInfo)) {
            if (!SessionUtils.isAdmin(request)) {
                response.sendRedirect(request.getContextPath());
            } else {
                request.getRequestDispatcher("/WEB-INF/jsp/template/newExposition.jsp").forward(request, response);
            }

        } else if ("/viewExposition".equals(pathInfo)) {
            request.getRequestDispatcher("/WEB-INF/jsp/template/viewExposition.jsp").forward(request, response);

        } else {
            request.getRequestDispatcher("/error").forward(request, response);
        }
    }
}
