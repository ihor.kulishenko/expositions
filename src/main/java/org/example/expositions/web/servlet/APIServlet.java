package org.example.expositions.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.expositions.delegate.*;
import org.example.expositions.dto.LoginDTO;
import org.example.expositions.dto.RegisterDTO;
import org.example.expositions.dto.ResultDTO;
import org.example.expositions.entity.Role;
import org.example.expositions.factory.*;
import org.example.expositions.util.ServletUtils;
import org.example.expositions.util.SessionUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Pattern;

public class APIServlet extends HttpServlet {

    private static final Logger logger = LogManager.getLogger();

    private static final Pattern expositionIdPattern = Pattern.compile("^/exposition/(?<expositionId>\\d+)/?$");
    private static final Pattern ticketSoldPattern = Pattern.compile("^/ticket/sold/(?<expositionId>\\d+)/?$");

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        final var pathInfo = request.getPathInfo();
        final var method = request.getMethod().toLowerCase();

        if ("/hall".equals(pathInfo) && "get".equals(method)) {
            handleGetHallsAction(response);
        } else if ("/exposition".equals(pathInfo) && "get".equals(method)) {
            handleGetExpositionsByFilter(request, response);
        } else if (pathInfo.startsWith("/exposition/") && "get".equals(method)) {
            handleGetExpositionInfoAction(request, response, pathInfo);
        } else if ("/user".equals(pathInfo) && "get".equals(method)) {
            handleGetUserInfoAction(request, response, pathInfo);
        } else if (pathInfo.startsWith("/ticket/sold/") && "get".equals(method)) {
            handleGetSoldTicketsAction(request, response, pathInfo);
        } else if ("/login".equals(pathInfo) && "post".equals(method)) {
            handleLoginAction(request, response);
        } else if ("/register".equals(pathInfo) && "post".equals(method)) {
            handleRegisterAction(request, response);
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

    }

    private void handleGetExpositionsByFilter(HttpServletRequest request, HttpServletResponse response) {
        final var filterDTO = ExpositionFilterFactory.createDTO(request);
        final var expositionFilter = ExpositionFilterFactory.createVO(filterDTO);

        final var expositionsResult = new ExpositionDelegate().getByFilter(expositionFilter);
        if (expositionsResult.failure()) {
            logger.error(expositionsResult.message);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        ServletUtils.sendResult(
                ResultDTO.ok(FilteredExpositionsFactory.createDTO(expositionsResult.container)), response);
    }

    private void handleGetHallsAction(HttpServletResponse response) {
        // get all halls
        final var result = new HallDelegate().getAll();

        if (result.failure()) {
            logger.error(result.message);

            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        ServletUtils.sendResult(ResultDTO.ok(HallFactory.createDTOList(result.container)), response);
    }

    private void handleGetExpositionInfoAction(HttpServletRequest request, HttpServletResponse response, String pathInfo) {
        try {
            final var matcher = expositionIdPattern.matcher(pathInfo);

            if (!matcher.find()) {
                logger.warn("exposition id not found in path: {}", pathInfo);
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            final var expositionIdStr = matcher.group("expositionId");
            final var expositionId = Long.parseLong(expositionIdStr);

            final var expositionResult = new ExpositionDelegate().getById(expositionId);
            if (expositionResult.failure()) {
                logger.error(expositionResult.message);
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            final var hallsResult = new HallDelegate().getByExposition(expositionResult.container);
            if (hallsResult.failure()) {
                logger.error(hallsResult.message);
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            final var scheduleResult = new ScheduleDelegate().getByExposition(expositionResult.container);
            if (scheduleResult.failure()) {
                logger.error(hallsResult.message);
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            ServletUtils.sendResult(ResultDTO.ok(ExpositionFactory.createDTO(SessionUtils.getLanguage(request),
                    expositionResult.container, hallsResult.container, scheduleResult.container)), response);

        } catch (IllegalStateException | NumberFormatException e) {
            logger.warn("no match found for string: " + pathInfo, e);
        }
    }

    private void handleGetUserInfoAction(HttpServletRequest request, HttpServletResponse response, String pathInfo) {
        final var user = SessionUtils.getCurrentUser(request);
        if (user == null) {
            logger.error("unauthorized request: {}", pathInfo);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        ServletUtils.sendResult(ResultDTO.ok(UserFactory.createDTO(user)), response);
    }

    private void handleGetSoldTicketsAction(HttpServletRequest request, HttpServletResponse response, String pathInfo) {
        if (SessionUtils.getCurrentUser(request) == null || SessionUtils.getCurrentUser(request).getRole() != Role.ADMINISTRATOR) {
            logger.error("unauthorized request: {}", pathInfo);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        try {
            final var matcher = ticketSoldPattern.matcher(pathInfo);
            if (!matcher.find()) {
                logger.warn("exposition id not found in path: {}", pathInfo);
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            final var expositionId = Long.parseLong(matcher.group("expositionId"));
            final var ticketsResult = new TicketDelegate().getTotalNumber(expositionId);

            if (ticketsResult.success()) {
                ServletUtils.sendOk(response, ticketsResult);
            } else {
                ServletUtils.sendError(response, ticketsResult.message);
            }

        } catch (IllegalStateException | NumberFormatException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void handleLoginAction(HttpServletRequest request, HttpServletResponse response) {
        final var login = ServletUtils.getDto(request, LoginDTO.class);

        final var checkUserResult = new UserDelegate().checkUser(login.getUsername(), login.getPassword());

        if (checkUserResult.success()) {
            logger.info("USER LOGGED IN: {}", checkUserResult.container);
            final var session = request.getSession(true);
            session.setAttribute(SessionUtils.ATTRIBUTES.user.name(), checkUserResult.container);
            ServletUtils.sendOk(response, checkUserResult);
        } else {
            ServletUtils.sendError(response, checkUserResult.message);
        }
    }

    private void handleRegisterAction(HttpServletRequest request, HttpServletResponse response) {
        final var register = ServletUtils.getDto(request, RegisterDTO.class);
        final var registerResult = new UserDelegate().create(register);
        if (registerResult.success()) {
            logger.info("USER CREATED: {}", registerResult.container);

            final var oldSession = request.getSession(false);
            if (oldSession != null) {
                oldSession.invalidate();
            }

            final var newSession = request.getSession(true);
            newSession.setAttribute(SessionUtils.ATTRIBUTES.user.name(), registerResult.container);

            ServletUtils.sendOk(response, registerResult);
        } else {
            ServletUtils.sendError(response, registerResult.message);
        }
    }
}
