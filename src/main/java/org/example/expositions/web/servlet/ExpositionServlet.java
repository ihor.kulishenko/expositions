package org.example.expositions.web.servlet;

import org.example.expositions.delegate.ExpositionDelegate;
import org.example.expositions.util.ServletUtils;
import org.example.expositions.util.SessionUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Exposition related entry point for web-clients
 */
public class ExpositionServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final var actionName = Optional.ofNullable(request.getParameter("action")).orElse("");

        if ("remove".equals(actionName)) {
            removeExpositionAction(request, response);
        } else {
            defaultAction(request, response);
        }
    }

    private void removeExpositionAction(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final var expositionId = request.getParameter("id");

        if (expositionId == null || !SessionUtils.isAdmin(request)) {
            response.sendRedirect(request.getContextPath());
            return;
        }

        final var inactivateResult = new ExpositionDelegate().inactivate(Long.parseLong(expositionId));
        ServletUtils.sendResult(ServletUtils.toResultDTO(inactivateResult), response);

    }

    private void defaultAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(request, response);
    }


    private enum attrs {
        isAuthorized, isAdmin, userName, error, expositions, dateFrom, dateTo, timeFrom, timeTo, halls,
        errorMessage, price, exposition, soldTickets, expositionFilter, expositionSortColumn, expositionSortOrder, paginator
    }
}
